﻿using Anno.EngineData;
using Anno.Plugs.DeployService.Tasks;
using Anno.ViperLog;
using System;
using Anno.Const;
using Anno.Util;

namespace Anno.Plugs.DeployService
{
    [DependsOn(
           typeof(ViperLogBootstrap)
           )]
    public class DeployBootStrap : IPlugsConfigurationBootstrap
    {
        public void ConfigurationBootstrap()
        {
            ServiceMonitorTask.Start();
        }

        public void PreConfigurationBootstrap()
        {
            string baseRoot = CustomConfiguration.Settings["work_directory"];
            string appName = SettingService.AppName;
            var annoProcesss = AnnoProcess.GetAnnoProcesses(baseRoot);
            if (annoProcesss != null)
            {
                annoProcesss.ForEach(service =>
                {
                    if (!service.NodeName.Equals(appName))
                    {
                        service.NodeName = appName;
                        AnnoProcess.WriteAnnoProcesseByWorkingDirectory(service, baseRoot);
                    }
                });
            }
        }
    }
}
