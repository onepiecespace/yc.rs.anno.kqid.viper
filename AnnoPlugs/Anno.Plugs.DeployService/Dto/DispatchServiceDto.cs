﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anno.Plugs.DeployService.Dto
{
    /// <summary>
    /// 调度出新的服务
    /// </summary>
    public class DispatchServiceDto
    {
        /// <summary>
        /// 被调度的服务名称
        /// </summary>
        public string WorkingName { get; set; }
        /// <summary>
        /// 接受调度的annoDeploy昵称
        /// </summary>
        public string ReceiverNodeName { get; set; }
        /// <summary>
        /// 接受调度的annoDeploy部署口令
        /// </summary>
        public string ReceiverdeploySecret { get; set; }
        
        /// <summary>
        /// 启动命令
        /// </summary>
        public string Cmd { get; set; }
        /// <summary>
        /// 新服务的WorkingName
        /// </summary>
        public string NewWorkingName { get; set; }
    }
}
