﻿using Anno.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace Anno.Plugs.DeployService.Dto
{
    public class AnnoProcessInfoDto : AnnoProcessInfo
    {
        public AnnoProcessInfoDto() { }
        public AnnoProcessInfoDto(AnnoProcessInfo ap, string StandardOutput, string StandardError)
        {
            this.StandardOutput = StandardOutput;
            this.StandardError = StandardError;
            Id = ap.Id;
            AnnoProcessDescription= ap.AnnoProcessDescription;
            WorkingDirectory= ap.WorkingDirectory;
            Cmd = ap.Cmd;
            NodeName = ap.NodeName;
            Running = ap.Running;
            AutoStart = ap.AutoStart;
            ReStartErrorCount = ap.ReStartErrorCount;

        }
        public string StandardOutput { get; set; } = string.Empty;
        public string StandardError { get; set; } = string.Empty;
    }
}
