﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anno.Plugs.DeployService.Dto
{
    public class DeployAgent
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 服务地址
        /// </summary>
        public string Host { get; set; }
        /// <summary>
        /// 服务端口
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// 超时时间毫秒
        /// </summary>
        public int Timeout { get; set; }
        /// <summary>
        /// 服务权重
        /// </summary>
        public int Weight { get; set; }
    }
}
