﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anno.Plugs.DeployService.Dto
{
    public class UpLoadFormData
    {
        public string workingDirectory { get; set; }
        public string nodeName { get; set; }
        public string cmd { get; set; }
        public string autoStart { get; set; }
        public string deploySecret { get; set; }
        public string annoProcessDescription { get; set; }
    }
}
