﻿using Anno.Const.Attribute;
using Anno.EngineData;
using Anno.Plugs.DeployService.Dto;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anno.Plugs.DeployService
{
    public class FileManagerModule : LogBaseModule
    {
        public FileManagerModule()
        {
        }
        /// <summary>
        /// 上传文件到服务器
        /// </summary>
        /// <returns></returns>
        [AnnoInfo(Desc = "上传文件到服务器")]
        public ActionResult UpLoadFile(UpLoadFormData formData)
        {
            if (!formData.deploySecret.Equals(Const.CustomConfiguration.Settings["deploySecret"]))
            {
                this.Warn($"{formData?.workingDirectory}-部署口令错误", "UpLoadFile");
                return new ActionResult(false, null, null, "部署口令错误！");
            }
            string currentRoot = formData.workingDirectory;
            if (currentRoot.ToLower().Equals("proc"))
            {
                this.Warn($"{formData?.workingDirectory}-proc为保留名称，请更改你的WorkingDirectory", "UpLoadFile");
                return new ActionResult(false, null, null, "proc为保留名称，请更改你的WorkingDirectory！");
            }
            var baseRoot = Const.CustomConfiguration.Settings["work_directory"];
            var annoFiles = GetAnnoFiles();
            if (annoFiles.Count > 0)
            {
                string pathRoot = GetRootName(annoFiles[0].FileName);
                if (string.IsNullOrEmpty(currentRoot))
                {
                    currentRoot = pathRoot;
                }
                #region 如果程序在运行则关闭
                var ap = Util.AnnoProcess.GetAnnoProcesseByWorkingDirectory(currentRoot, baseRoot);
                if (ap != null)
                {
                    if (Util.Bash.Native)
                    {
                        Util.Bash.Linux.KillProcessAndChildren(ap.Id);
                    }
                    else
                    {
                        Util.Bash.Windows.KillProcessAndChildren(ap.Id);
                    }
                }
                #endregion
                #region 创建文件夹
                foreach (var file in annoFiles)
                {
                    string fileFullName = Path.Combine(baseRoot, currentRoot, file.FileName.Substring(pathRoot.Length + 1));
                    var fileName = GetFileName(file.FileName);
                    string fileParent = fileFullName.Substring(0, fileFullName.Length - fileName.Length - 1);
                    if (!Directory.Exists(fileParent))
                    {
                        Directory.CreateDirectory(fileParent);
                    }
                }
                #endregion
                Parallel.ForEach(annoFiles, file =>
                {
                    string fileFullName = Path.Combine(baseRoot, currentRoot, file.FileName.Substring(pathRoot.Length + 1));
                    FileMode fileMode = FileMode.Create;
                    if (File.Exists(fileFullName))
                    {
                        fileMode = FileMode.Truncate;
                    }
                    using (var stream =new  FileStream(fileFullName, fileMode, FileAccess.ReadWrite))
                    {
                        stream.Write(file.Content, 0, file.Length);
                    }
                });
                Tasks.ServiceMonitorTask.NeedGc = true;
            }
            else
            {
                string fileParent = Path.Combine(baseRoot, currentRoot);
                if (!Directory.Exists(fileParent))
                {
                    Directory.CreateDirectory(fileParent);
                }
                //更新的时候可以不更新文件
                #region 如果程序在运行则关闭
                var ap = Util.AnnoProcess.GetAnnoProcesseByWorkingDirectory(currentRoot, baseRoot);
                if (ap != null)
                {
                    if (Util.Bash.Native)
                    {
                        Util.Bash.Linux.KillProcessAndChildren(ap.Id);
                    }
                    else
                    {
                        Util.Bash.Windows.KillProcessAndChildren(ap.Id);
                    }
                }
                #endregion
            }
            if (formData.autoStart.Equals("1"))
            {
                Task.Run(() =>
                {
                    Util.BashResult bashResult = null;
                    if (Util.Bash.Native)
                    {
                        bashResult = Util.Bash.Linux.StartProcess(formData.cmd, Path.Combine(baseRoot, currentRoot));
                    }
                    else
                    {
                        bashResult = Util.Bash.Windows.StartProcess(formData.cmd, Path.Combine(baseRoot, currentRoot));
                    }
                    if (bashResult != null)
                    {
                        var annoProcessInfo = new Util.AnnoProcessInfo()
                        {
                            Cmd = formData.cmd,
                            NodeName = Const.SettingService.AppName,
                            WorkingDirectory = currentRoot,
                            Id = bashResult.Id,
                            Running = true,
                            AnnoProcessDescription = formData.annoProcessDescription,
                            AutoStart = formData.autoStart
                        };
                        Util.AnnoProcess.WriteAnnoProcesseByWorkingDirectory(annoProcessInfo, baseRoot);
                    }
                });
            }
            else
            {
                var annoProcessInfo = new Util.AnnoProcessInfo()
                {
                    Cmd = formData.cmd,
                    NodeName = Const.SettingService.AppName,
                    WorkingDirectory = currentRoot,
                    Id = -1,
                    Running = false,
                    AnnoProcessDescription = formData.annoProcessDescription,
                    AutoStart=formData.autoStart
                };
                Util.AnnoProcess.WriteAnnoProcesseByWorkingDirectory(annoProcessInfo, baseRoot);
            }
            this.Info($"{formData?.workingDirectory}-上传成功", "UpLoadFile");
            return new ActionResult(true, null, null, "上传成功");
        }

        private List<AnnoFile> GetAnnoFiles()
        {
            var annoFiles = new ConcurrentQueue<AnnoFile>();
            Parallel.ForEach(this.Input, file =>
            {
                if (file.Key.StartsWith("annoDeploy___"))
                {
                    var annoFile = Newtonsoft.Json.JsonConvert.DeserializeObject<AnnoFile>(file.Value);
                    annoFiles.Enqueue(annoFile);
                }
            });
            return annoFiles.ToList();
        }

        private string GetFileName(string fileFullName)
        {
            string fileName = null;
            if (!string.IsNullOrWhiteSpace(fileFullName))
            {
                var ld = fileFullName.LastIndexOf("/");
                var wd = fileFullName.LastIndexOf("\\");
                var fileSplit = Math.Max(ld, wd) + 1;
                if (fileSplit < fileFullName.Length)
                {
                    fileName = fileFullName.Substring(fileSplit);
                }
            }
            return fileName;
        }

        private string GetRootName(string uploadFileName)
        {
            string rootName = null;
            if (!string.IsNullOrWhiteSpace(uploadFileName))
            {
                var ld = uploadFileName.IndexOf("/");
                var wd = uploadFileName.IndexOf("\\");
                if (ld < 0 && wd < 0)
                {
                    return rootName;
                }
                var fileSplit = Math.Min(ld, wd);
                if (fileSplit <= 0 && ld < 0)
                {
                    fileSplit = wd;
                }
                else if (fileSplit <= 0 && wd < 0)
                {
                    fileSplit = ld;
                }
                if (fileSplit > 0)
                {
                    rootName = uploadFileName.Substring(0, fileSplit);
                }
            }
            return rootName;
        }
    }
}
