﻿using Anno.Const.Attribute;
using Anno.EngineData;
using Anno.Plugs.DeployService.Dto;
using Anno.Plugs.DeployService.Filter;
using Anno.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anno.Plugs.DeployService
{
    public class DeployManagerModule : LogBaseModule
    {
        private readonly IRpcConnector connector;
        public DeployManagerModule(IRpcConnector connector)
        {
            this.connector = connector;
        }
        #region AnnoDeploy结点管理
        /// <summary>
        /// AnnoDeploy集合
        /// </summary>
        /// <returns></returns>
        [AnnoInfo(Desc = "AnnoDeploy集合")]
        public ActionResult GetDeployServices()
        {
            List<DeployAgent> deployAgents = new List<DeployAgent>();
            var microList = Rpc.Client.Connector.Micros.Where(m => m.Tags.Contains("Anno.Plugs.Deploy")).Select(m => m.Mi).ToList();
            if (microList != null && microList.Count > 0)
            {
                foreach (var service in microList)
                {
                    if (deployAgents.Any(it => it.Host == service.Ip && it.Port == service.Port))
                    {
                        continue;
                    }
                    DeployAgent deployAgent = new DeployAgent();
                    deployAgent.Host = service.Ip;
                    deployAgent.Port = service.Port;
                    deployAgent.Timeout = service.Timeout;
                    deployAgent.Weight = service.Weight;
                    deployAgent.Nickname = service.Nickname;

                    deployAgents.Add(deployAgent);
                }
            }
            return new ActionResult(true, deployAgents);
        }

        /// <summary>
        /// 微服务调度{workingName:被调度的服务名称}，{receiverNodeName：接受调度的annoDeploy昵称}
        /// 只能调度到除原结点{xxx}之外的其他结点
        /// </summary>
        /// <param name="dispatch"></param>
        /// <returns></returns>
        [AnnoInfo(Desc = "微服务调度{workingName:被调度的服务名称}，{receiverNodeName：接受调度的annoDeploy昵称}")]
        public ActionResult DispatchService([FromBody]DispatchServiceDto dispatch)
        {
            if (dispatch.ReceiverNodeName.Equals(Const.SettingService.AppName))
            {
                return new ActionResult(false, null, null, $"只能调度到除原结点{dispatch.ReceiverNodeName}之外的其他结点。");
            }
            var baseRoot = Const.CustomConfiguration.Settings["work_directory"];
            var annoProcesse = AnnoProcess.GetAnnoProcesseByWorkingDirectory(dispatch.WorkingName, baseRoot);
            if (annoProcesse == null)
            {
                return new ActionResult(false, null, null, $"workingName:{dispatch.WorkingName},服务未找到。");
            }
            Dictionary<string, string> inputData = new Dictionary<string, string>();
            inputData.Add(Const.Enum.Eng.NAMESPACE, "Anno.Plugs.Deploy");
            inputData.Add(Const.Enum.Eng.CLASS, "FileManager");
            inputData.Add(Const.Enum.Eng.METHOD, "UpLoadFile");
            UpLoadFormData formData = new UpLoadFormData()
            {
                annoProcessDescription = annoProcesse.AnnoProcessDescription,
                autoStart = annoProcesse.AutoStart,
                cmd = annoProcesse.Cmd,
                deploySecret = dispatch.ReceiverdeploySecret,
                nodeName = dispatch.ReceiverNodeName,
                workingDirectory = annoProcesse.WorkingDirectory
            };
            if (!string.IsNullOrWhiteSpace(dispatch.Cmd)) {
                formData.cmd = dispatch.Cmd;
            }
            if (!string.IsNullOrWhiteSpace(dispatch.NewWorkingName)) {
                formData.workingDirectory = dispatch.NewWorkingName;
            }
            inputData.Add("formData", Newtonsoft.Json.JsonConvert.SerializeObject(formData));
            var deployFiles = Utils.DirectoryFiles.GetDirectoryFiles(Path.Combine(baseRoot, dispatch.WorkingName));
            foreach (var dFile in deployFiles)
            {
                dFile.Value.FileName = $"annoDeploy___{dispatch.WorkingName}/{dFile.Value.FileName}";
                inputData.Add(dFile.Value.FileName, Newtonsoft.Json.JsonConvert.SerializeObject(dFile.Value));
            }
            var rlt = connector.BrokerDnsAsync(inputData, dispatch.ReceiverNodeName).ConfigureAwait(false).GetAwaiter().GetResult();
            var aR = Newtonsoft.Json.JsonConvert.DeserializeObject<ActionResult<string>>(rlt);

            Tasks.ServiceMonitorTask.NeedGc = true;
            if (aR.Status)
            {
                return new ActionResult(true, null, null, $"服务：{dispatch.WorkingName}调度完成，请切换到{dispatch.ReceiverNodeName}结点查看。");
            }
            else
            {
                return new ActionResult(false, null, null, aR.Msg);
            }
        }
        #endregion

        #region 管理结点服务
        /// <summary>
        /// 获取此Node上面的服务
        /// </summary>
        /// <returns></returns>
        [AnnoInfo(Desc = "获取此Node上面的服务")]
        public ActionResult GetServices()
        {
            var baseRoot = Const.CustomConfiguration.Settings["work_directory"];
            var annoProcesses = AnnoProcess.GetAnnoProcesses(baseRoot);
            return new ActionResult(true, annoProcesses.OrderByDescending(p=>p.Running).ThenBy(p=>p.WorkingDirectory));
        }

        /// <summary>
        /// 获取此Node上面的服务
        /// </summary>
        /// <returns></returns>
        [AnnoInfo(Desc = "获取此Node上面的服务")]
        public ActionResult GetServiceByWorkingName(string workingName)
        {
            var msgLength = 10240;
            var baseRoot = Const.CustomConfiguration.Settings["work_directory"];
            var annoProcesse = AnnoProcess.GetAnnoProcesseByWorkingDirectory(workingName, baseRoot);
            string standardOutput = string.Empty
                , standardError = string.Empty;
            try
            {
                string logFile = Path.Combine(Directory.GetCurrentDirectory(), "serviceLog", $"{workingName}_StandardOutput.log");
                if (File.Exists(logFile))
                {
                    using (FileStream fs = new FileStream(logFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        if (fs.Length < msgLength)
                        {
                            standardOutput = sr.ReadToEnd();
                        }
                        else
                        {
                            byte[] buffer = new byte[msgLength];
                            fs.Seek(-buffer.Length, SeekOrigin.End);
                            fs.Read(buffer, 0, buffer.Length);
                            standardOutput = Encoding.UTF8.GetString(buffer);
                        }
                    }
                }
            }
            catch { }
            try
            {
                string logFile = Path.Combine(Directory.GetCurrentDirectory(), "serviceLog", $"{workingName}_StandardError.log");
                if (File.Exists(logFile))
                {
                    using (FileStream fs = new FileStream(logFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        if (fs.Length < msgLength)
                        {
                            standardError = sr.ReadToEnd();
                        }
                        else
                        {
                            byte[] buffer = new byte[msgLength];
                            fs.Seek(-buffer.Length, SeekOrigin.End);
                            fs.Read(buffer, 0, buffer.Length);
                            standardError = Encoding.UTF8.GetString(buffer);
                        }
                    }
                }
            }
            catch { }
            return new ActionResult(true, new AnnoProcessInfoDto(annoProcesse,
                standardOutput,
                standardError));
        }
        /// <summary>
        /// 根据工作目录停止服务
        /// </summary>
        /// <returns></returns>
        [AnnoInfo(Desc = "根据工作目录停止服务")]
        [DeployAuthorization]
        public ActionResult StopServiceByWorkingDirectory(string workingName)
        {
            var baseRoot = Const.CustomConfiguration.Settings["work_directory"];
            #region 如果程序在运行则关闭
            var ap = AnnoProcess.GetAnnoProcesseByWorkingDirectory(workingName, baseRoot);
            if (ap != null)
            {
                if (Bash.Native)
                {
                    Bash.Linux.KillProcessAndChildren(ap.Id);
                }
                else
                {
                    Bash.Windows.KillProcessAndChildren(ap.Id);
                }
                ap.Running = false;
                ap.Id = -1;
                AnnoProcess.WriteAnnoProcesseByWorkingDirectory(ap, baseRoot);
                this.Info($"{workingName}-[{ap.Id}]-停止服务", "AnnoDeploy");
                return new ActionResult(true);
            }
            #endregion
            this.Error($"{workingName}-未找到指定的服务", "AnnoDeploy");
            return new ActionResult(false, null, null, "未找到指定的服务");
        }

        /// <summary>
        /// 根据工作目录启动或重启服务
        /// </summary>
        /// <returns></returns>
        [AnnoInfo(Desc = "根据工作目录启动或重启服务")]
        [DeployAuthorization]
        public ActionResult StartServiceByWorkingDirectory(string workingName)
        {
            var baseRoot = Const.CustomConfiguration.Settings["work_directory"];
            var ap = AnnoProcess.GetAnnoProcesseByWorkingDirectory(workingName, baseRoot);
            string errorMsg = "未找到指定的服务";
            if (ap != null)
            {
                #region 如果程序在运行则关闭
                if (Bash.Native)
                {
                    Bash.Linux.KillProcessAndChildren(ap.Id);
                }
                else
                {
                    Bash.Windows.KillProcessAndChildren(ap.Id);
                }
                #endregion
                #region 启动
                try
                {
                    BashResult bashResult = null;
                    if (Bash.Native)
                    {
                        bashResult = Bash.Linux.StartProcess(ap.Cmd, Path.Combine(baseRoot, ap.WorkingDirectory));
                    }
                    else
                    {
                        bashResult = Bash.Windows.StartProcess(ap.Cmd, Path.Combine(baseRoot, ap.WorkingDirectory));
                    }

                    if (bashResult.Id > 0)
                    {
                        ap.Running = true;
                        ap.Id = bashResult.Id;
                    }
                    else
                    {
                        ap.Running = false;
                        errorMsg = bashResult.ErrorMsg;
                    }
                    AnnoProcess.WriteAnnoProcesseByWorkingDirectory(ap, baseRoot);
                }
                catch (Exception ex)
                {
                    errorMsg = ex.Message;
                    AnnoProcess.WriteAnnoProcesseByWorkingDirectory(ap, baseRoot);
                }
                #endregion
                this.Info($"{workingName}-[{ap?.Id}]-启动或重启服务", "AnnoDeploy");
                return new ActionResult(true, null, null, errorMsg);
            }
            this.Error($"{workingName}-{errorMsg}", "AnnoDeploy");
            return new ActionResult(false, null, null, errorMsg);
        }

        /// <summary>
        /// 根据工作目录停止和移除服务
        /// </summary>
        /// <returns></returns>
        [AnnoInfo(Desc = "根据工作目录停止和移除服务")]
        [DeployAuthorization]
        public ActionResult StopAndRemoveServiceByWorkingDirectory(string workingName)
        {
            var baseRoot = Const.CustomConfiguration.Settings["work_directory"];
            var ap = AnnoProcess.GetAnnoProcesseByWorkingDirectory(workingName, baseRoot);

            if (ap != null)
            {
                #region 如果程序在运行则关闭
                if (Bash.Native)
                {
                    Bash.Linux.KillProcessAndChildren(ap.Id);
                }
                else
                {
                    Bash.Windows.KillProcessAndChildren(ap.Id);
                }
                AnnoProcess.RemoveAnnoProcesseByWorkingDirectory(workingName, baseRoot);
                #endregion
                #region 清理服务
                //目录
                string processWorking = Path.Combine(baseRoot, workingName);
                if (Directory.Exists(processWorking))
                {
                    var deleTarget = new DirectoryInfo(processWorking);
                    deleTarget.Attributes = FileAttributes.Normal;
                    Directory.Delete(processWorking, true);
                }
                #endregion
                this.Info($"{workingName}-[{ap?.Id}]-停止和移除服务", "AnnoDeploy");
                return new ActionResult(true);
            }
            this.Error($"{workingName}-未找到指定的服务", "AnnoDeploy");
            return new ActionResult(false, null, null, "未找到指定的服务");
        }
        #endregion
    }
}
