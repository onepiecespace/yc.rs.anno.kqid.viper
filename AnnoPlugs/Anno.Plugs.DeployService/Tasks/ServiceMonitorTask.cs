﻿using Anno.CronNET;
using Anno.Util;
using Anno.ViperLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Anno.Plugs.DeployService.Tasks
{
    public class ServiceMonitorTask
    {
        /// <summary>
        /// 需要执行Gc任务
        /// </summary>
        public static volatile bool NeedGc = false;
        /// <summary>
        /// 每个10秒检测一遍服务
        /// </summary>
        private static CronDaemon cron_daemon = new CronDaemon();
        static ServiceMonitorTask()
        {
            cron_daemon.AddJob("*/30 * * * * ? *", MonitorTask);
            cron_daemon.AddJob("*/30 * * * * ? *", GcTask);
        }
        /// <summary>
        /// 启动任务
        /// </summary>
        internal static void Start()
        {
            lock (cron_daemon)
            {
                cron_daemon.Start();
            }
        }
        /// <summary>
        /// 停止任务
        /// </summary>
        internal static void Stop()
        {
            lock (cron_daemon)
            {
                cron_daemon.Stop();
            }
        }
        /// <summary>
        /// 服务检测
        /// </summary>
        private static void MonitorTask()
        {
            try
            {
                var baseRoot = Const.CustomConfiguration.Settings["work_directory"];
                var annoProcesses = AnnoProcess.GetAnnoProcesses(baseRoot);
                if (annoProcesses != null && annoProcesses.Count > 0)
                {
                    Parallel.ForEach(annoProcesses, proc =>
                    {
                        //WorkingDirectory 以Uoo 开始的为仅此执行一次的命令，不做守护
                        if (!proc.WorkingDirectory.StartsWith("Uoo_") && proc != null && proc.Id > 0 && proc.ReStartErrorCount < 3 && proc.Running == false)
                        {
                            ReStartService(proc);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                try
                {
                    Loader.IocLoader.Resolve<IViperLogService>().Error(new { Exception = ex }, "ServiceMonitorTask", "AnnoDeploy", "ServiceMonitorTask");
                }
                catch { }
            }
        }

        private static void ReStartService(AnnoProcessInfo ap)
        {
            var baseRoot = Const.CustomConfiguration.Settings["work_directory"];

            if (ap != null)
            {
                #region 如果程序在运行则关闭
                if (Bash.Native)
                {
                    Bash.Linux.KillProcessAndChildren(ap.Id);
                }
                else
                {
                    Bash.Windows.KillProcessAndChildren(ap.Id);
                }
                #endregion
                #region 启动
                try
                {
                    BashResult bashResult = null;
                    if (Bash.Native)
                    {
                        bashResult = Bash.Linux.StartProcess(ap.Cmd, Path.Combine(baseRoot, ap.WorkingDirectory));
                    }
                    else
                    {
                        bashResult = Bash.Windows.StartProcess(ap.Cmd, Path.Combine(baseRoot, ap.WorkingDirectory));
                    }
                    if (bashResult.Id > 0)
                    {
                        var checkCount = 20;
                        Process proc = null;
                        while (checkCount > 0)
                        {
                            Task.Delay(1000).Wait();
                            proc = Process.GetProcessById(bashResult.Id);
                            if (proc == null || proc.HasExited||checkCount-- <= 0)
                            {
                                break;
                            }
                        }
                        //程序属于异常退出
                        if (proc == null || proc.HasExited)
                        {
                            ap.Running = false;
                            ap.ReStartErrorCount++;
                            Loader.IocLoader.Resolve<IViperLogService>().Fatal($"异常恢复失败-{ap.WorkingDirectory}", "ServiceMonitorTask", "AnnoDeploy", "ServiceMonitorTask");
                        }
                        else
                        {
                            ap.Running = true;
                            ap.Id = bashResult.Id;
                            ap.ReStartErrorCount = 0;

                            Loader.IocLoader.Resolve<IViperLogService>().Fatal($"异常恢复成功-{ap.WorkingDirectory}", "ServiceMonitorTask", "AnnoDeploy", "ServiceMonitorTask");
                        }
                    }
                    AnnoProcess.WriteAnnoProcesseByWorkingDirectory(ap, baseRoot);
                }
                catch (Exception ex)
                {
                    ap.ReStartErrorCount++;
                    AnnoProcess.WriteAnnoProcesseByWorkingDirectory(ap, baseRoot);
                    try
                    {
                        Loader.IocLoader.Resolve<IViperLogService>().Error(new { Appservice = ap, Exception = ex }, "ServiceMonitorTask", "AnnoDeploy", "ServiceMonitorTask");
                    }
                    catch { }
                }
                #endregion
            }
        }

        /// <summary>
        /// 根据信号GC
        /// </summary>
        private static void GcTask()
        {
            if (NeedGc)
            {
                GC.Collect();
                Task.Delay(1000).Wait();
                GC.Collect();
                NeedGc = false;
            }
        }
    }
}
