﻿using Anno.EngineData;
using Anno.EngineData.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Anno.Plugs.DeployService.Filter
{
    /// <summary>
    /// 服务权限过滤器（需要跟参数带有口令 deploySecret）
    /// </summary>
    public class DeployAuthorization : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(BaseModule context)
        {
            /*
             * 需要部署口令
             */
            if (context.RequestString("deploySecret")!=null&&!context.RequestString("deploySecret").Equals(Const.CustomConfiguration.Settings["deploySecret"]))
            {
                context.Authorized = false;
                context.ActionResult= new ActionResult(false, null, null, "部署口令错误！");
                return;
            }
            context.Authorized = true;
        }
    }
}
