﻿using Anno.Plugs.DeployService.Dto;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anno.Plugs.DeployService.Utils
{
    public static class DirectoryFiles
    {
        /// <summary>
        /// 根据文件夹路径递归获取子文件路径
        /// </summary>
        /// <param name="dirPath">文件夹根目录</param>
        /// <returns></returns>
        public static ConcurrentDictionary<string, AnnoFile> GetDirectoryFiles(string rootDir, string childDir = null)
        {
            if (string.IsNullOrWhiteSpace(rootDir))
            {
                throw new ArgumentNullException("给定的文件夹路不能为空");
            }
            if (childDir == null)
            {
                childDir = rootDir;
            }
            int rootDirLength = rootDir.Length;
            ConcurrentDictionary<string, AnnoFile> files = new ConcurrentDictionary<string, AnnoFile>();
            DirectoryInfo root = new DirectoryInfo(childDir);
            if (root.Exists)
            {
                Parallel.ForEach(root.GetFiles(), new ParallelOptions() { MaxDegreeOfParallelism = 5 }, file =>
                {
                    using (FileStream fsRead = new FileStream(file.FullName, FileMode.Open, FileAccess.Read))
                    {
                        string fileName = file.FullName.Substring(rootDirLength + 1);
                        fileName = fileName.TrimStart(Path.DirectorySeparatorChar);
                        BinaryReader objReader = new BinaryReader(fsRead);
                        //读取文件内容
                        byte[] byteFile = objReader.ReadBytes((int)fsRead.Length);
                        var afile = new AnnoFile()
                        {
                            ContentType = "application/octet-stream",
                            ContentDisposition = "annoDeploy",
                            Content = byteFile,
                            FileName = fileName,
                            Length = byteFile.Length,
                            Name = file.Name
                        };
                        files.TryAdd(fileName, afile);
                    }
                });

                Parallel.ForEach(root.GetDirectories(), new ParallelOptions() { MaxDegreeOfParallelism = 5 }, dir =>
                {
                    var _files = GetDirectoryFiles(rootDir, dir.FullName);
                    if (_files != null && _files.Count > 0)
                    {
                        foreach (var f in _files)
                        {
                            files.TryAdd(f.Key, f.Value);
                        }
                    }
                });

            }
            else
            {
                throw new ArgumentNullException("给定的文件夹路径不存在");
            }
            return files;
        }
    }

}
