﻿/****************************************************** 
Writer: Du YanMing-admin
Mail:dym880@163.com
Create Date: 8/31/2021 4:01:22 PM
Functional description： ServiceControl
******************************************************/
using Anno.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Anno.Deploy
{
    public static class ServiceControl
    {

        /// <summary>
        /// 根据参数 启动服务
        /// </summary>
        /// <param name="args"></param>
        public static void Start(string[] args)
        {
            AnnoLog.WriteLogIsBackground= true;

            InitConfig();
            string baseRoot = Const.CustomConfiguration.Settings["work_directory"];
            var workingName = Rpc.Server.ArgsValue.GetValueByName("start", args);
            if (string.IsNullOrWhiteSpace(workingName))
            {
                Log.Log.WriteLine("!!! 没有指定程序名称", ConsoleColor.Yellow);
                return;
            }
            workingName = workingName.Trim();
            var ap = AnnoProcess.GetAnnoProcesseByWorkingDirectory(workingName, baseRoot);
            if (ap != null)
            {
                #region 如果程序在运行则关闭
                if (Bash.Native)
                {
                    Bash.Linux.KillProcessAndChildren(ap.Id);
                }
                else
                {
                    Bash.Windows.KillProcessAndChildren(ap.Id);
                }
                #endregion
                #region 启动
                string errorMsg = "未找到指定的服务";
                try
                {
                    BashResult bashResult = null;
                    if (Bash.Native)
                    {
                        bashResult = Bash.Linux.StartProcess(ap.Cmd, Path.Combine(baseRoot, ap.WorkingDirectory));
                    }
                    else
                    {
                        bashResult = Bash.Windows.StartProcess(ap.Cmd, Path.Combine(baseRoot, ap.WorkingDirectory));
                    }

                    if (bashResult.Id > 0)
                    {
                        ap.Running = true;
                        ap.Id = bashResult.Id;
                    }
                    else
                    {
                        ap.Running = false;
                        errorMsg = bashResult.ErrorMsg;
                    }
                    AnnoProcess.WriteAnnoProcesseByWorkingDirectory(ap, baseRoot);
                    if (ap.Running)
                    {
                        Log.Log.WriteLine($"{workingName} 已启动！");
                    }
                    else
                    {
                        Log.Log.WriteLine($"{workingName} 启动失败！{errorMsg}");
                    }
                    WriteLineProcessesInfo(new List<AnnoProcessInfo>() { ap });
                }
                catch (Exception ex)
                {
                    errorMsg = ex.Message;
                    AnnoProcess.WriteAnnoProcesseByWorkingDirectory(ap, baseRoot);
                    Log.Log.WriteLine($"{workingName} 启动失败！{errorMsg}");
                }
                #endregion
            }
            else
            {
                Log.Log.WriteLine($"!!! 没有找到程序 {workingName}", ConsoleColor.Yellow);
            }
            Task.Delay(5000).Wait();
        }
        /// <summary>
        /// 根据参数 停止服务
        /// </summary>
        /// <param name="args"></param>
        public static void Stop(string[] args)
        {
            InitConfig();
            string baseRoot = Const.CustomConfiguration.Settings["work_directory"];
            var workingName = Rpc.Server.ArgsValue.GetValueByName("stop", args);
            if (string.IsNullOrWhiteSpace(workingName))
            {
                Log.Log.WriteLine("!!! 没有指定程序名称", ConsoleColor.Yellow);
                return;
            }
            workingName = workingName.Trim();
            var ap = AnnoProcess.GetAnnoProcesseByWorkingDirectory(workingName, baseRoot);
            if (ap != null)
            {
                if (Bash.Native)
                {
                    Bash.Linux.KillProcessAndChildren(ap.Id);
                }
                else
                {
                    Bash.Windows.KillProcessAndChildren(ap.Id);
                }
                ap.Running = false;
                ap.Id = -1;
                AnnoProcess.WriteAnnoProcesseByWorkingDirectory(ap, baseRoot);
                Log.Log.WriteLine($"{workingName} 已停止！");
                WriteLineProcessesInfo(new List<AnnoProcessInfo>() { ap });
            }
            else
            {
                Log.Log.WriteLine($"!!! 没有找到程序 {workingName}", ConsoleColor.Yellow);
            }
        }
        /// <summary>
        /// 根据参数 查看服务状态
        /// </summary>
        /// <param name="args"></param>
        public static void Status(string[] args)
        {
            InitConfig();
            string baseRoot = Const.CustomConfiguration.Settings["work_directory"];
            WriteLineProcessesInfo(AnnoProcess.GetAnnoProcesses(baseRoot));
        }

        private static void WriteLineProcessesInfo(List<AnnoProcessInfo> annoProcesses)
        {
            if (annoProcesses == null) { return; }
            foreach (var item in annoProcesses)
            {
                string uptime = string.Empty.PadRight(26);
                if (item.Running && item.Id > 0)
                {
                    var process = System.Diagnostics.Process.GetProcessById(item.Id);
                    if (process != null && !process.HasExited)
                    {
                        TimeSpan uptime_span = DateTime.Now - process.StartTime;
                        uptime = $"uptime {uptime_span.Days} days,{uptime_span.Hours.ToString().PadLeft(2,'0')}:{uptime_span.Minutes.ToString().PadLeft(2, '0')}:{uptime_span.Seconds.ToString().PadLeft(2, '0')},".PadRight(26, ' ');
                    }
                }
                Log.Log.WriteLineNoDate($"{item.WorkingDirectory.PadRight(30, ' ')} {(item.Running == true ? "UP".PadRight(5, ' ') : "DOWN".PadRight(5, ' '))} pid {item.Id.ToString().PadRight(5, ' ')},{uptime}{item.AnnoProcessDescription}");
            }
        }
        private static void InitConfig()
        {
            Const.SettingService.InitConfig();
        }
    }
}
