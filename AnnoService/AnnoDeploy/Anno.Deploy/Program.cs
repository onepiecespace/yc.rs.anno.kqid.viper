﻿using Anno.Loader;
using Autofac;
using System;
using System.Linq;

namespace Anno.Deploy
{
    using Anno.EngineData;
    using Anno.Rpc.Server;
    using System.Collections.Generic;
    using System.Reflection;

    class Program
    {
        static void Main(string[] args)
        {
            #region DeployUtil
            if (args.Contains("start"))
            {
                ServiceControl.Start(args);
                return;
            }
            if (args.Contains("stop"))
            {
                ServiceControl.Stop(args);
                return;
            }
            if (args.Contains("status"))
            {
                ServiceControl.Status(args);
                return;
            }
            if (args.Contains("-help"))
            {
                Log.Log.WriteLineNoDate(@"
启动参数：
                -p   6659                    设置启动端口
                -xt  200                     设置服务最大线程数
                -t   20000                   设置超时时间（单位毫秒）
                -w   1                       设置权重
                -h   192.168.0.2             设置服务在注册中心的地址
                -tr  false                   设置调用链追踪是否启用
");
                return;
            }
            #endregion
            #region 功能插件（单文件）
            List<Assembly> assemblys = new List<Assembly>();
            Plugs.DeployService.DeployBootStrap deployBoot=new Plugs.DeployService.DeployBootStrap();
            assemblys.Add(deployBoot.GetType().Module.Assembly);
            Plugs.MonitorService.Bootstrap monitorBoot = new Plugs.MonitorService.Bootstrap();
            assemblys.Add(monitorBoot.GetType().Module.Assembly);
            #endregion
            /**
             * 启动默认DI库为 Autofac 可以切换为微软自带的DI库 DependencyInjection
             */
            Bootstrap.StartUp(args, () =>//服务配置文件读取完成后回调(服务未启动)
            {
                Const.SettingService.TraceOnOff = false;
                var autofac = IocLoader.GetAutoFacContainerBuilder();
                autofac.RegisterType(typeof(RpcConnectorImpl)).As(typeof(IRpcConnector)).SingleInstance();
            }
            , () =>//服务启动后的回调方法
            {
                /**
                 * 服务Api文档写入注册中心
                 */
                Bootstrap.ApiDoc();
            }, IocType.Autofac);
        }
    }
}
