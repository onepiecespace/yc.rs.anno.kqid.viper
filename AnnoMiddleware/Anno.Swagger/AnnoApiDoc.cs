﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anno.Swagger
{

    public class ActionResult<T> {

        /// <summary>
        /// 状态
        /// </summary>
        public Boolean Status { get; set; }
        /// <summary>
        /// 消息
        /// </summary>
        public string Msg { get; set; }
        /// <summary>
        /// 字典
        /// </summary>
        public Dictionary<string, object> Output { get; set; }
        /// <summary>
        /// 结果集
        /// </summary>
        public T OutputData { get; set; }
    }
    public class AnnoApiDoc
    {
        /// <summary>
        /// 来自哪个App
        /// </summary>
        public string App { get; set; }

        public string Channel { get; set; }
        public string Router { get; set; }
        public string Method { get; set; }

        public string Desc { get; set; }
        public DataValue Value { get; set; }
    }

    public class DataValue
    {
        public string Name { get; set; }
        public string Desc { get; set; }
        public List<ParametersValue> Parameters { get; set; }
    }
    public class ParametersValue
    {
        public string Name { get; set; }
        public string Desc { get; set; }
        public int Position { get; set; }
        public string ParameterType { get; set; }

    }
}
