﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Text;

namespace Anno.Util
{
    public class Windows
    {
        /// <summary>Stores output of the previous command if redirected.</summary>
        public string Output { get; private set; }

        /// <summary>
        /// Gets an array of the command output split by newline characters if redirected. </summary>
        public string[] Lines => Output?.Split(Environment.NewLine.ToCharArray());

        /// <summary>Stores the exit code of the previous command.</summary>
        public int ExitCode { get; private set; }

        /// <summary>Stores the error message of the previous command if redirected.</summary>
        public string ErrorMsg { get; private set; }
        /// <summary>Execute a new Bash command.</summary>
        /// <param name="input">The command to execute.</param>
        ///<param name="workingDirectory">workingDirectory</param>
        /// <param name="redirect">Print output to terminal if false.</param>
        /// <returns>A `BashResult` containing the command's output information.</returns>
        public BashResult Command(string input, string workingDirectory, bool redirect)
        {
            if (Bash.Native)
                throw new Exception("请切换到Linux.");

            int pid = 0;
            using (var bash = new Process { StartInfo = BashInfo(input, workingDirectory, redirect) })
            {
                bash.Start();
                pid = bash.Id;
                if (redirect)
                {
                    Output = bash.StandardOutput.ReadToEnd()
                        .TrimEnd(Environment.NewLine.ToCharArray());
                    ErrorMsg = bash.StandardError.ReadToEnd()
                        .TrimEnd(Environment.NewLine.ToCharArray());

                }
                else
                {
                    Output = null;
                    ErrorMsg = null;
                }

                if (bash.WaitForExit(1000))
                {
                    ExitCode = bash.ExitCode;
                }
                bash.Close();
            }

            if (redirect)
                return new BashResult(Output, ErrorMsg, ExitCode, pid);
            else
                return new BashResult(null, null, ExitCode, pid);
        }

        public BashResult StartProcess(string input, string workingDirectory)
        {
            Process process = null;
            try
            {
                input = input.Trim();
                var fileNameLength = input.IndexOf(" ");
                string processName = fileNameLength == -1 ? input : input.Substring(0, fileNameLength);
                string arguments = fileNameLength >= 0 ? input.Substring(fileNameLength) : "";
                string fileName = Path.Combine(workingDirectory, processName);
                if (!File.Exists(fileName))
                {
                    fileName = processName;
                }
                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    FileName = fileName,
                    Arguments = arguments.TrimStart(),
                    WorkingDirectory = workingDirectory,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };
                process = Process.Start(startInfo);
                string[] workingName = workingDirectory.Split(Path.DirectorySeparatorChar);
                if (workingName.Length > 0)
                {

                    AnnoLog.WriteLog(process, workingName[workingName.Length - 1]);
                }
            }
            catch (Exception ex)
            {
                return new BashResult(null, ex.Message, -1, -1);
            }
            return new BashResult(null, null, ExitCode, process.Id);
        }
        /// <summary>
        /// 功能：根据父进程id，杀死与之相关的进程树
        /// </summary>
        /// <param name="pid">传入参数：父进程id</param>

        public void KillProcessAndChildren(int pid)
        {
            if (pid < 0)
                return;
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ParentProcessID=" + pid);
                ManagementObjectCollection moc = searcher.Get();
                foreach (ManagementObject mo in moc)
                {
                    KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
                }

                Process proc = Process.GetProcessById(pid);
                proc.Kill();
            }
            catch (Exception)
            {
                /* process already exited */
            }
        }

        public BashResult Command(string input, bool redirect)
        {
            return this.Command(input, null, redirect);
        }
        public BashResult Command(string input)
        {
            return this.Command(input, true);
        }
        private ProcessStartInfo BashInfo(string input, string workingDirectory, bool redirectOutput)
        {
            Console.InputEncoding = Encoding.UTF8;
            var processStartInfo = new ProcessStartInfo
            {
                FileName = Bash._bashPath,
                Arguments = $"/c \"{input}\"",
                RedirectStandardInput = redirectOutput,
                RedirectStandardOutput = redirectOutput,
                RedirectStandardError = redirectOutput,
                UseShellExecute = false,
                CreateNoWindow = true,
                ErrorDialog = false
            };
            if (!string.IsNullOrWhiteSpace(workingDirectory))
            {
                processStartInfo.WorkingDirectory = workingDirectory;
            }
            return processStartInfo;
        }
        /// <summary>Echo the given string to standard output.</summary>
        /// <param name="input">The string to print.</param>
        /// <param name="redirect">Print output to terminal if false.</param>
        /// <returns>A `BashResult` containing the command's output information.</returns>
        public BashResult Echo(string input, bool redirect = false) =>
            Command($"echo {input}", redirect: redirect);

        /// <summary>Echo the given string to standard output.</summary>
        /// <param name="input">The string to print.</param>
        /// <param name="flags">Optional `echo` arguments.</param>
        /// <param name="redirect">Print output to terminal if false.</param>
        /// <returns>A `BashResult` containing the command's output information.</returns>
        public BashResult Echo(string input, string flags, bool redirect = false) =>
            Command($"echo {flags} {input}", redirect: redirect);

        /// <summary>Echo the given string to standard output.</summary>
        /// <param name="input">The string to print.</param>
        /// <param name="redirect">Print output to terminal if false.</param>
        /// <returns>A `BashResult` containing the command's output information.</returns>
        public BashResult Echo(object input, bool redirect = false) =>
            Command($"echo {input}", redirect: redirect);

        /// <summary>Echo the given string to standard output.</summary>
        /// <param name="input">The string to print.</param>
        /// <param name="flags">Optional `echo` arguments.</param>
        /// <param name="redirect">Print output to terminal if false.</param>
        /// <returns>A `BashResult` containing the command's output information.</returns>
        public BashResult Echo(object input, string flags, bool redirect = false) =>
            Command($"echo {flags} {input}", redirect: redirect);


        /// <summary>List information about files in the current directory.</summary>
        /// <param name="redirect">Print output to terminal if false.</param>
        /// <returns>A `BashResult` containing the command's output information.</returns>
        public BashResult Dir(bool redirect = true) =>
            Command("dir", redirect: redirect);
    }
}
