﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Anno.Util
{
    public static class AnnoLog
    {
        public static object locker = new object();

        /// <summary>
        /// 后台线程写日志文件
        /// </summary>
        public static bool WriteLogIsBackground { get; set; } = false;

        public static void WriteLog(Process process, string serviceName)
        {
            (new Thread(() =>
            {
                AnnoServiceLog(process, serviceName, LogType.StandardOutput);
            })
            { IsBackground = WriteLogIsBackground }).Start();


            (new Thread(() =>
            {
                AnnoServiceLog(process, serviceName, LogType.StandardError);
            })
            { IsBackground = WriteLogIsBackground }).Start();
        }
        /// <summary>
        /// Anno服务日志
        /// </summary>
        /// <param name="process"></param>
        /// <param name="serviceName"></param>
        /// <param name="logType"></param>
        static void AnnoServiceLog(Process process, string serviceName, LogType logType)
        {
            try
            {
                string dir = Path.Combine(Directory.GetCurrentDirectory(), "serviceLog");
                lock (locker)
                {
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                }
                var fileName = Path.Combine(dir, $"{serviceName}_{logType}.log");
                if (File.Exists(fileName))
                {
                    new FileStream(fileName, FileMode.Truncate, FileAccess.ReadWrite).Close();
                }
                process.EnableRaisingEvents = true;
                CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
                CancellationToken cancellationToken = cancellationTokenSource.Token;
                process.Exited += new EventHandler((object sender, EventArgs e) =>
                {
                    WriteLogFile(fileName, $" [{DateTime.Now:HH:mm:ss:ffff}]: 服务已停止！");
                    cancellationTokenSource.Cancel(false);
                    process.Dispose();
                });
                File.Create(fileName).Close();
                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        if (logType.Equals(LogType.StandardError))
                        {
                            if (!process.StandardError.EndOfStream)
                            {
                                string msg = process.StandardError.ReadLine();
                                if (msg != null)
                                {
                                    WriteLogFile(fileName, msg);
                                }
                            }
                            else
                            {
                                Task.Delay(500).Wait();
                            }
                        }
                        else if (logType.Equals(LogType.StandardOutput))
                        {
                            if (!process.StandardOutput.EndOfStream)
                            {
                                string msg = process.StandardOutput.ReadLine();
                                if (msg != null)
                                {
                                    WriteLogFile(fileName, msg);
                                }
                            }
                            else
                            {
                                Task.Delay(500).Wait();
                            }
                        }
                    }
                    catch (ThreadAbortException)
                    {
                        cancellationTokenSource.Cancel(false);
                        process.Dispose();
                        break;
                    }
                    catch (Exception) { }
                }
            }
            finally
            {
                process.Close();
            }
        }
        static void WriteLogFile(string fileName, string msg)
        {
            try
            {
                using (FileStream file = new FileStream(fileName, FileMode.Append))
                using (StreamWriter writer = new StreamWriter(file, Encoding.UTF8))
                {
                    writer.WriteLine(msg, writer);
                    writer.Flush();
                    writer.Close();
                    file.Close();
                }
            }
            catch { }//允许丢失日志
        }
    }

    internal enum LogType
    {
        StandardOutput,
        StandardError
    }
}
