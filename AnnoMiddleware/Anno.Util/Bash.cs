﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Anno.Util
{
    /// <summary>Handles boilerplate for Bash commands and stores output information.</summary>
    public class Bash
    {
        private static bool _linux { get; }
        private static bool _mac { get; }
        private static bool _windows { get; }
        internal static string _bashPath { get; }

        /// <summary>Determines whether bash is running in a native OS (Linux/MacOS).</summary>
        /// <returns>True if in *nix, else false.</returns>
        public static bool Native { get; }


        static Bash()
        {
            _linux = RuntimeInformation.IsOSPlatform(OSPlatform.Linux);
            _mac = RuntimeInformation.IsOSPlatform(OSPlatform.OSX);
            _windows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

            Native = _linux || _mac ? true : false;
            _bashPath = Native ? "/bin/bash" : "cmd.exe";
            if (!File.Exists(_bashPath) && Native)
            {
                if (File.Exists("bin/sh"))
                {
                    _bashPath = "bin/sh";
                }
                else if (File.Exists("/usr/bin/sh"))
                {
                    _bashPath = "/usr/bin/sh";
                }
                else if (File.Exists("/usr/bin/bash"))
                {
                    _bashPath = "/usr/bin/bash";
                }
            }
        }
        /// <summary>
        /// 当前程序路径
        /// </summary>
        public static string CurrentExePath
        {
            get
            {
                var path = Process.GetCurrentProcess().MainModule.FileName;
                return path.Substring(0, path.LastIndexOf("/"));
            }
        }

        public static Linux Linux { get { return new Linux(); } }

        public static Windows Windows { get { return new Windows(); } }
    }
}

