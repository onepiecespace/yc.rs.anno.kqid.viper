﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anno.Util
{
    public class AnnoProcessInfo
    {
        public AnnoProcessInfo()
        {
        }
        /// <summary>
        /// PID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 工作目录文件夹名称
        /// </summary>
        public string WorkingDirectory { get; set; }

        /// <summary>
        /// 启动命令
        /// </summary>
        public string Cmd { get; set; }
        /// <summary>
        /// 集群节点名称（Anno.Deploy 名称）
        /// </summary>

        public string NodeName { get; set; }

        /// <summary>
        /// 是否运行中
        /// </summary>
        public bool Running { get; set; } = false;
        /// <summary>
        /// 1 自动运行
        /// </summary>
        public string AutoStart { get; set; } = "1";
        /// <summary>
        /// 服务描述
        /// </summary>
        public string AnnoProcessDescription { get; set; } = string.Empty;
        /// <summary>
        /// 重启失败次数
        /// </summary>
        public int ReStartErrorCount { get; set; } = 0;

    }
}
