﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Anno.Util
{
    public static class AnnoProcess
    {
        /// <summary>
        /// Anno进程锁
        /// </summary>
        private static object _locker = new object();
        /// <summary>
        /// 获取工作目录下所有的程序信息
        /// </summary>
        /// <param name="baseWorking">基础工作目录</param>
        /// <returns></returns>
        public static List<AnnoProcessInfo> GetAnnoProcesses(string baseWorking)
        {
            List<AnnoProcessInfo> annoProcesses = new List<AnnoProcessInfo>();
            //目录
            string proc = Path.Combine(baseWorking, "proc");
            if (Directory.Exists(proc))
            {
                var files = Directory.GetFiles(proc);
                foreach (var file in files)
                {
                    var _process = new FileInfo(file);
                    var process = GetAnnoProcesseByWorkingDirectory(_process.Name, baseWorking);
                    if (proc != null)
                    {
                        annoProcesses.Add(process);
                    }
                }
            }
            return annoProcesses;
        }
        /// <summary>
        /// 根据基础工作目录和程序工作目录获取程序信息
        /// </summary>
        /// <param name="workingDirectory">程序工作目录</param>
        /// <param name="baseWorking">基础工作目录</param>
        /// <returns></returns>
        public static AnnoProcessInfo GetAnnoProcesseByWorkingDirectory(string workingDirectory, string baseWorking)
        {
            AnnoProcessInfo annoProcess = null;
            lock (_locker)
            {
                try
                {
                    //目录
                    string processFile = Path.Combine(baseWorking, "proc", workingDirectory);
                    using (FileStream file = new FileStream(processFile, FileMode.Open, FileAccess.Read))
                    {

                        using (StreamReader reader = new StreamReader(file))
                        {
                            annoProcess = Newtonsoft.Json.JsonConvert.DeserializeObject<AnnoProcessInfo>(reader.ReadToEnd());
                        }
                    }
                }
                catch { }
            }
            try
            {
                if (annoProcess != null)
                {
                    if (annoProcess.Id > 0)
                    {
                        Process proc = Process.GetProcessById(annoProcess.Id);
                        //程序属于异常退出
                        if (proc == null || proc.HasExited)
                        {
                            annoProcess.Running = false;
                        }
                    }
                    else if (annoProcess.Id <= 0)
                    {
                        annoProcess.Running = false;
                    }
                }
            }
            catch
            {
                //程序属于异常退出
                if (annoProcess != null)
                {
                    annoProcess.Running = false;
                }
            }
            return annoProcess;
        }
        /// <summary>
        /// 根据基础工作目录和程序工作目录移除程序
        /// </summary>
        /// <param name="workingDirectory">程序工作目录</param>
        /// <param name="baseWorking">基础工作目录</param>
        /// <returns></returns>
        public static bool RemoveAnnoProcesseByWorkingDirectory(string workingDirectory, string baseWorking)
        {
            lock (_locker)
            {
                //目录
                string processFile = Path.Combine(baseWorking, "proc", workingDirectory);
                var fileInfo = new FileInfo(processFile);
                fileInfo.Attributes = FileAttributes.Normal;
                fileInfo.Delete();
            }
            return true;
        }
        /// <summary>
        /// 更新或者写入程序信息
        /// </summary>
        /// <param name="annoProcess">程序信息</param>
        /// <param name="baseWorking">基础工作目录</param>
        /// <returns></returns>
        public static bool WriteAnnoProcesseByWorkingDirectory(AnnoProcessInfo annoProcess, string baseWorking)
        {
            //目录
            string processFile = Path.Combine(baseWorking, "proc", annoProcess.WorkingDirectory);
            lock (_locker)
            {
                if (!Directory.Exists(Path.Combine(baseWorking, "proc")))
                {
                    Directory.CreateDirectory(Path.Combine(baseWorking, "proc"));
                }
                FileMode fileMode = FileMode.Create;
                if (File.Exists(processFile))
                {
                    fileMode = FileMode.Truncate;
                }
                using (FileStream file = new FileStream(processFile, fileMode, FileAccess.ReadWrite))
                {
                    using (StreamWriter writer = new StreamWriter(file, Encoding.UTF8))
                    {
                        writer.Write(Newtonsoft.Json.JsonConvert.SerializeObject(annoProcess));
                        writer.Flush();
                    }
                }
            }
            return true;
        }

    }
}
