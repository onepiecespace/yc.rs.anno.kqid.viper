﻿using System;

namespace Anno.Const.Enum
{
    /// <summary>
    /// Redis 缓存业务 常量
    /// </summary>
    public static class RedisExpiryConst
    {
        /// <summary>
        /// 登录缓存过期时间
        /// </summary>
        public const int LoginExpiryDate = 720;
        public const int SmsExpiryDate=720;//需改为5分钟

    }
}
