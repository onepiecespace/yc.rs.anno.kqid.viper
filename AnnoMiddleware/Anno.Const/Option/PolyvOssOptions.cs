﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anno.Const
{
    public static class PolyvOssOptions
    {
        /// <summary>
        /// 访问域名 
        /// </summary>
        public static string writetoken { get; set; }
        public static JSONRPC JSONRPC { get; set; }
    }
    public  class JSONRPC
    {
        /// <summary>
        /// title
        /// </summary>
        public  string title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public  string tag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public  string desc { get; set; }
    }
}
