﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anno.Const
{
    public static class AliyunOssOptions
    {
        /// <summary>
        /// 访问域名 
        /// </summary>
        public static string EndPoint { get; set; }
        /// <summary>
        /// Bucket
        /// </summary>
        public static string Bucket { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public static string AccessKeyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public static string AccessKeySecret { get; set; }
        /// <summary>
        /// 存储空间名称
        /// </summary>
        public static string BucketName { get; set; }
        public static string PrefixPath { get; set; }
        public static string Host { get; set; }
        public static string PreviewHost { get; set; }
        public static bool UseHttps { get; set; }
    }
}
