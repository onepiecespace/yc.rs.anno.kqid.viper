﻿using Anno.Const;
using SqlSugar;
using System;

namespace Anno.Infrastructure
{
    /// <summary>
    /// 数据库实例
    /// </summary>
    public static class DbInstance
    {
        /// <summary>
        /// 数据库默认类型为MySql
        /// </summary>
        private static DbType DbType = DbType.MySql;
        static DbInstance()
        {
            if (CustomConfiguration.Settings.ContainsKey("DbType"))
            {
                DbType = (DbType)Enum.Parse(typeof(DbType), CustomConfiguration.Settings["DbType"]);
            }
        }
        /// <summary>
        /// SqlSugarClient 微服务控制中心仓储
        /// </summary>
        public static SqlSugarClient Db => new SqlSugarClient(new ConnectionConfig()
        {
            ConnectionString = AppSettings.ConnStr, //必填
            DbType = DbType, //必填
            IsAutoCloseConnection = true, //默认false
            InitKeyType = InitKeyType.SystemTable //默认SystemTable
        });
        /// <summary>
        /// SqlSugarClient 多租户权限仓储
        /// </summary>
        public static SqlSugarClient SaasSysDb => new SqlSugarClient(new ConnectionConfig()
        {
            ConnectionString = AppSettings.SaasSysConnStr, //必填
            DbType = DbType, //必填
            IsAutoCloseConnection = true, //默认false//为true表示可以自动删除二级缓存 
            InitKeyType = InitKeyType.SystemTable //默认SystemTable
        });
        /// <summary>
        /// SqlSugarClient 成长树仓储
        /// </summary>
        public static SqlSugarClient GrowthDb => new SqlSugarClient(new ConnectionConfig()
        {
            ConnectionString = AppSettings.GrowthConnStr, //必填
            DbType = DbType, //必填
            IsAutoCloseConnection = true, //默认false 是否自动释放数据库，设为true我们不需要close或者Using的操作，比较推荐
            InitKeyType = InitKeyType.SystemTable //默认SystemTable
        });
    }
}
