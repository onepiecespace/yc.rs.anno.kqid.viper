# 🎄Viper
    Viper 是一个基于Anno微服务引擎开发的Dashboard项目、示例项目。Anno 底层通讯采用 grpc、thrift。自带服务发现、调用链追踪、Cron 调度、限流、事件总线等等

## 🎁Nuget 扩展

Package name                           |Description     | Version                     | Downloads
---------------------------------------|----------------|-----------------------------|------------------------
`Anno.EventBus`|EventBus事件总线库 | [![NuGet](https://img.shields.io/nuget/v/Anno.EventBus.svg?style=flat-square&label=nuget)](https://www.nuget.org/packages/Anno.EventBus/) | ![downloads](https://img.shields.io/nuget/dt/Anno.EventBus.svg)
`Anno.RateLimit`|令牌桶、漏桶限流库 | [![NuGet](https://img.shields.io/nuget/v/Anno.RateLimit.svg?style=flat-square&label=nuget)](https://www.nuget.org/packages/Anno.RateLimit/) | ![downloads](https://img.shields.io/nuget/dt/Anno.RateLimit.svg)
`Anno.EngineData.RateLimit` |Anno服务限流中间件| [![NuGet](https://img.shields.io/nuget/v/Anno.EngineData.RateLimit.svg?style=flat-square&label=nuget)](https://www.nuget.org/packages/Anno.EngineData.RateLimit/) | ![downloads](https://img.shields.io/nuget/dt/Anno.EngineData.RateLimit.svg)
`Anno.LRUCache`|缓存库 | [![NuGet](https://img.shields.io/nuget/v/Anno.LRUCache.svg?style=flat-square&label=nuget)](https://www.nuget.org/packages/Anno.LRUCache/) | ![downloads](https://img.shields.io/nuget/dt/Anno.LRUCache.svg)
`Anno.EngineData.Cache`|Anno服务缓存中间件 | [![NuGet](https://img.shields.io/nuget/v/Anno.EngineData.Cache.svg?style=flat-square&label=nuget)](https://www.nuget.org/packages/Anno.EngineData.Cache/) | ![downloads](https://img.shields.io/nuget/dt/Anno.EngineData.Cache.svg)
`Anno.Plugs.MonitorService`|Anno服务监控中间件 | [![NuGet](https://img.shields.io/nuget/v/Anno.Plugs.MonitorService.svg?style=flat-square&label=nuget)](https://www.nuget.org/packages/Anno.Plugs.MonitorService/) | ![downloads](https://img.shields.io/nuget/dt/Anno.Plugs.MonitorService.svg)
`Anno.Rpc.Client.DynamicProxy`|接口代理Anno客户端扩展 | [![NuGet](https://img.shields.io/nuget/v/Anno.Rpc.Client.DynamicProxy.svg?style=flat-square&label=nuget)](https://www.nuget.org/packages/Anno.Rpc.Client.DynamicProxy/) | ![downloads](https://img.shields.io/nuget/dt/Anno.Rpc.Client.DynamicProxy.svg)
`Anno.Const`|配置库 | [![NuGet](https://img.shields.io/nuget/v/Anno.Const.svg?style=flat-square&label=nuget)](https://www.nuget.org/packages/Anno.Const/) | ![downloads](https://img.shields.io/nuget/dt/Anno.Const.svg)

## 🎂整体架构
![整体架构](https://s3.ax1x.com/2020/12/18/rtegcd.png)

整体架构主要分为三个部分

　　1、注册中心：AnnoCenter 

　　2、微服务：AnnoService（可以是多个服务例如：订单服务A、库存服务B、支付服务C、用户服务D）

　　3、ApiGateway：[参考Viper](https://github.com/duyanming/Viper)

　
## 🎂目录说明

整体目录架构主要分为9个部分

　　00、服务网关+运维监控平台：ApiGateway：[参考Viper](https://github.com/duyanming/Viper)
   　01、 注册中心服务：ViperCenter（服务注册、发现、健康检查、KV存储、API文档、负载均衡中心，端口：7010（AnnoCenter）已启动！）
　　02、注册微服务基础支撑服务Host：ViperService（可以是多个基础支撑服务例如：分布式锁服务（Anno.Plugs.DLockService）、插件解析服务（Anno.Plugs.MonitorService）、监控服务（Anno.Plugs.LogicService）、跟踪服服务（Anno.Plugs.TraceService））
　   03、注册微服务业务中心微服务Host：MicroServices（可以是多个服务例如：订单服务A、库存服务B、支付服务C、用户服务D）
　　04、注册微服务基础权限模块： BaseServices （可以十多个服务例如:自动任务（JobService）,权限框架（PermissionService），多租户系统用户体系（SaasSysService））
　　05、注册微服务中台业务模块： BusinessServices （可以十多个服务例如:成长树模块（GrowthService）,订单中心（OrderingService），支付中心（PayMentService），产品中心（ProductService））
　　06、模块服务之间远程调用领域实体： ModulePlugDto （各个模块远程调用服务DTO）
       07、监控基础服务的组成模块： Component
       08、公共服务中间组件： ServiceCommon       数据业务处理模块中心： Trace（数据持久化服务层）
       09、微服务引擎： AnnoCore(底层通可选用 grpc或thrift。自带服务发现、调用链追踪、Cron 调度、限流、事件总线、CQRS 、DDD、插件化开发)
       10、平台业务多租户权限接口： PlatformService
       11 综合信息服务中心： 待定
# 📢主要功能

　　服务注册中心、服务发现、健康检查、负载均衡、限流、失败重试、链路追踪、资源监控等功能


