﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using System;
using System.Collections.Generic;
using System.Text;

namespace Anno.Common.Aliyun
{
    /// <summary>
    /// 阿里云短信服务接口
    /// </summary>
    public class SmsHelper
    {
        //产品名称:云通信短信API产品,开发者无需替换
        const String product = "Dysmsapi";
        //产品域名,开发者无需替换
        const String domain = "dysmsapi.aliyuncs.com";

        // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
        const String accessKeyId = "LTAI5tNkzniq1ecX7PXEHMUu";//LTAI4G9MasTM4JbP9NuTwjQ9
        const String accessKeySecret = "BFmecMbksCShqNq0LjHDUx5foIeDgg";//5aGvekNKYf3DItbPIRIIgc286ydc3Q

        /// <summary>
        /// 短信发送接口
        /// </summary>
        /// <param name="index">短信模板索引，1：登录验证码，2：身份验证验证码，3：登录确认验证码,4：登录异常验证码，5：用户注册验证码，6：修改密码验证码，7：信息变更验证码</param>
        /// <param name="mobile">手机号码</param>
        /// <param name="code">验证码（如果是验证码短信）</param>
        public static void SendSms(int index, string mobile, string code)
        {
            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();
            SendSmsResponse response;
            try
            {

                //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
                request.PhoneNumbers = mobile;
                //必填:短信签名-可在短信控制台中找到
                request.SignName = "两个黄鹂";
                //必填:短信模板-可在短信控制台中找到
                request.TemplateCode = GetSmsCode(index);
                //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                request.TemplateParam = "{\"code\":\"" + code + "\"}";
                //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
                request.OutId = "";
                //请求失败这里会抛ClientException异常
                response = acsClient.GetAcsResponse(request);

            }
            catch (ServerException e)
            {
                Console.WriteLine(e.ErrorCode);
            }
            catch (ClientException e)
            {
                Console.WriteLine(e.ErrorCode);
            }

        }

        /// <summary>
        /// 获取短信模板设置编号
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private static string GetSmsCode(int index)
        {
            string strCode = "";
            switch (index)
            {
                case 1: strCode = "SMS_235475148"; break;//两个黄鹂成长树-用户登录
                case 2: strCode = "SMS_235480107"; break;//两个黄鹂成长树-校长添加教务
                case 3: strCode = "SMS_234414945"; break;//两个黄鹂成长树-教务添加老师
                case 4: strCode = "SMS_235480108"; break; //两个黄鹂成长树 - 变更学员状态，给校长发送短信
                case 5: strCode = "SMS_235475151"; break;//两个黄鹂成长树-学员被分班，给校长发送短信
                case 6: strCode = "SMS_235480112"; break;//两个黄鹂成长树-家长添加家庭成员
                case 7: strCode = "SMS_235480117"; break;//两个黄鹂成长树-教务编辑老师
            }
            return strCode;
        }
    }
}
