﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Anno.Common.Util
{
    public class RandomNumberUtils
    {
        /// <summary>
        ///  防止创建类的实例
        /// </summary>
        private RandomNumberUtils() { }
        private static readonly object Locker = new object();
        private static int _sn = 0;

        /// <summary>
        /// 生成订单编号
        /// </summary>
        /// <returns></returns>
        public static string GenerateId(string mark, int num)
        {
            lock (Locker)   //lock 关键字可确保当一个线程位于代码的临界区时，另一个线程不会进入该临界区。
            {
                if (_sn == int.MaxValue)
                {
                    _sn = 0;
                }
                else
                {
                    _sn++;
                }

                Thread.Sleep(100);

                return mark + DateTime.Now.ToString("yyyyMMdd") + _sn.ToString().PadLeft(num, '0');
            }
        }
        /// <summary>
        /// 生成班级编号
        /// </summary>
        /// <returns></returns>
        public static string GenerateClassCode(string mark, int? year, int? quarter, int? classLevel,int? weekday)
        {
    
            return mark + year + quarter + classLevel + weekday;

        }
        public static string GenerateNo(string mark)
        {
            return mark + DateTime.Now.ToString("yyyyMMdd") + "000";

        }

        public static string markNo(string mark)
        {
            return mark + DateTime.Now.ToString("yyyy") + "000";

        }
        public static string GetLastStr(string str, int num)
        {
            int count = 0;
            if (str.Length > num)
            {
                count = str.Length - num;
                str = str.Substring(count, num);
            }
            return str;
        }
        /// <summary>
        ///生成制定位数的随机码（数字）
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GenerateRandomCode(int length)
        {
            var result = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var r = new Random(Guid.NewGuid().GetHashCode());
                result.Append(r.Next(0, 10));
            }
            return result.ToString();
        }
    }
}
