﻿using System;
using System.Globalization;

namespace Anno.Common.Util
{
    public static class DateTimeUtils
    {
        #region 将Unix时间戳转为C#格式时间

        /// <summary>
        /// 时间戳转为C#格式时间
        /// </summary>
        /// <param name="timeStamp">Unix时间戳格式</param>
        /// <returns>C#格式时间</returns>
        [Obsolete]
        public static DateTime GetTime(string timeStamp)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(timeStamp + "0000000");
            TimeSpan toNow = new TimeSpan(lTime);
            return dtStart.Add(toNow);
        }

        #endregion

        #region DateTime时间格式转换为Unix时间戳格式

        /// <summary>
        /// DateTime时间格式转换为Unix时间戳格式
        /// </summary>
        /// <param name="time"> DateTime时间格式</param>
        /// <returns>Unix时间戳格式</returns>
        [Obsolete]
        public static int ConvertDateTimeInt(DateTime time)
        {
            if (time == null || time == DateTime.MinValue)
            {
                return 0;
            }
            else
            {
                DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
                return (int)(time - startTime).TotalSeconds;
            }
        }

        #endregion
        ///   <summary> 
        ///  获取某一日期是该年中的第几周
        ///   </summary> 
        ///   <param name="dateTime"> 日期 </param> 
        ///   <returns> 该日期在该年中的周数 </returns> 
        public static int GetWeekOfYear(this DateTime dateTime)
        {
            GregorianCalendar gc = new GregorianCalendar();
            return gc.GetWeekOfYear(dateTime, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
        }

        /// <summary>
        /// 获取Js格式的timestamp
        /// </summary>
        /// <param name="dateTime">日期</param>
        /// <returns></returns>
        public static long ToJsTimestamp(this DateTime dateTime)
        {
            var startTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
            long result = (dateTime.Ticks - startTime.Ticks) / 10000;   //除10000调整为13位
            return result;
        }
        public static long CurrentTimeStamp(bool isMinseconds = false)
        {
            var ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 8, 0, 0, 0);
            long times = Convert.ToInt64(isMinseconds ? ts.TotalMilliseconds : ts.TotalSeconds);
            return times;
        }

        /// <summary>
        /// 获取js中的getTime()
        /// </summary>
        /// <param name="dt">日期</param>
        /// <returns></returns>
        public static Int64 JsGetTime(this DateTime dt)
        {
            Int64 retval = 0;
            var st = new DateTime(1970, 1, 1);
            TimeSpan t = (dt.ToUniversalTime() - st);
            retval = (Int64)(t.TotalMilliseconds + 0.5);
            return retval;
        }

        /// <summary>
        /// 返回默认时间1970-01-01
        /// </summary>
        /// <param name="dt">时间日期</param>
        /// <returns></returns>
        public static DateTime Default(this DateTime dt)
        {
            return DateTime.Parse("1970-01-01");
        }



        /// <summary>
        /// 转为本地时间
        /// </summary>
        /// <param name="time">时间</param>
        /// <returns></returns>
        public static DateTime ToLocalTime(this DateTime time)
        {
            return TimeZoneInfo.ConvertTime(time, TimeZoneInfo.Local);
        }

        /// <summary>
        /// 转为转换为Unix时间戳格式(精确到秒)
        /// </summary>
        /// <param name="time">时间</param>
        /// <returns></returns>
        public static int ToUnixTimeStamp(this DateTime time)
        {
            DateTime startTime = new DateTime(1970, 1, 1).ToLocalTime();
            return (int)(time - startTime).TotalSeconds;
        }
        //计算时间的差值
        public static string DateDiff(DateTime DateTime1, DateTime DateTime2)
        {
            string dateDiff = null;
            TimeSpan ts1 = new TimeSpan(DateTime1.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime2.Ticks);

            TimeSpan ts = ts1.Subtract(ts2).Duration();

            dateDiff = ts.Days.ToString() + "天" + ts.Hours.ToString() + "小时" + ts.Minutes.ToString() + "分钟";// +ts.Seconds.ToString() + "秒";
            return dateDiff;
        }
        public static int DayDiff(DateTime DateTime1, DateTime DateTime2)
        {
            int dateDiff = 0;
            TimeSpan ts1 = new TimeSpan(DateTime1.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            dateDiff = ts.Days;
            return dateDiff;
        }
        public static int MonthDiff(DateTime DateTime1, DateTime DateTime2)
        {
            int dateDiff = 0;
            TimeSpan ts1 = new TimeSpan(DateTime1.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            dateDiff = ts.Days;
            return dateDiff;
        }
        public static int minutesDiff(DateTime DateTime1, DateTime DateTime2)
        {
            TimeSpan span = DateTime1 - DateTime2;
            return (int)Math.Floor(span.TotalMinutes);
        }
        public static int minutesDiff2(DateTime DateTime1, DateTime DateTime2)
        {
            int dateDiff = 0;
            TimeSpan ts1 = new TimeSpan(DateTime1.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            dateDiff = ts.Minutes;
            return dateDiff;
        }
        public static TimeSpan secondsDiff(DateTime DateTime1, DateTime DateTime2)
        {
            TimeSpan ts1 = new TimeSpan(DateTime1.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            return ts;
        }
        public static DateTime dateMinute(DateTime DateTime)
        {
            string datetime = DateTime.ToString("yyyy-MM-dd HH:mm:00"); // 08:05:57
            DateTime ts = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, DateTime.Hour, DateTime.Minute, 00);//2012-12-2
            return ts;
        }
        /// <summary>
        /// 日期比较
        /// </summary>
        /// <param name="today">当前日期</param>
        /// <param name="writeDate">输入日期</param>
        /// <param name="n">比较天数</param>
        //<returns>大于天数返回true，小于返回false</returns>
        public static bool CompareDate(string today, string writeDate, int n)
        {
            DateTime Today = Convert.ToDateTime(today);
            DateTime WriteDate = Convert.ToDateTime(writeDate);
            WriteDate = WriteDate.AddDays(n);
            if (Today >= WriteDate)
                return false;
            else
                return true;
        }
        public static string DateStringFromDayMonth(DateTime dt)
        {
            return string.Format("{0}月{1}日", dt.Month, dt.Day);
        }
        public static string DateStringFromHHmm(DateTime dt)
        {
            return string.Format("{0}:{1}", dt.Hour, dt.Minute);
        }
        public static string DateStringFrom(DateTime? dt)
        {
            DateTime dt2 = Convert.ToDateTime(dt);
            return string.Format("{0}年{1}月{2}日 {3}:{4}", dt2.Year, dt2.Month, dt2.Day, dt2.Hour, dt2.Minute);
        }
        public static string DateStringFromNow(DateTime dt)
        {
            //TimeSpan span = DateTime.Now - dt;

            //Weekdays week = (Weekdays)CalWeekday(dt.Year, dt.Month, dt.Day);
            return string.Format("{0}-{1}-{2} {3}:{4}", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute);
            //if (span.TotalDays > 60)
            //{
            //    return dt.ToShortDateString();
            //}
            //else if (span.TotalDays > 30)
            //{
            //    return "1个月前";
            //}
            //else if (span.TotalDays > 14)
            //{
            //    return
            //    "2周前";
            //}
            //else if (span.TotalDays > 7)
            //{
            //    return "1周前";
            //}

            //if (span.TotalDays > 1)
            //{
            //    return string.Format("{0}年{1}月{3}日({4}){5}点{6}分", dt.Year, dt.Month, dt.Day, week, dt.Hour, dt.Minute);
            //}
            //else if (span.TotalHours > 1)
            //{
            //    return string.Format("{0}小时前|{1}", (int)Math.Floor(span.TotalHours), week);
            //}
            //else if (span.TotalMinutes > 1)
            //{
            //    return string.Format("{0}分钟前|{1}", (int)Math.Floor(span.TotalMinutes), week);
            //}
            //else if (span.TotalSeconds >= 1)
            //{
            //    return string.Format("{0}秒前|{1}",
            //    (int)Math.Floor(span.TotalSeconds), week);
            //}
            //else
            //{
            //    return string.Format("1秒前|{0}", week);

            //}
        }
        /// <summary>
        /// 枚举类型   string[] Day = new string[] { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
        /// </summary>
        enum Weekdays : short
        {
            星期日,
            星期一,
            星期二,
            星期三,
            星期四,
            星期五,
            星期六
        }
        /// <summary>
        /// 根据年月日来计算那一天是星期几
        /// </summary>
        /// <param name="y">年</param>
        /// <param name="m">月</param>
        /// <param name="d">日</param>
        /// <returns></returns>
        public static int CalWeekday(int y, int m, int d)
        {
            //基姆拉尔森公式
            return (d + 2 * m + 3 * (m + 1) / 5 + y + y / 4 - y / 100 + y / 400 + 1) % 7;
        }
        public static int? getquarter(int month)
        {
            int quarter = 0; // 1 春 2暑 3秋 4 寒
            switch (month)
            {
                case 1:
                    Console.WriteLine("{0}月是冬季！", month);
                    quarter = 4;
                    break;
                case 2:
                    Console.WriteLine("{0}月是冬季！", month);
                    quarter = 4;
                    break;
                case 3:
                    Console.WriteLine("{0}月是春季！", month);
                    quarter = 1;
                    break;
                case 4:
                    Console.WriteLine("{0}月是春季！", month);
                    quarter = 1;
                    break;
                case 5:
                    Console.WriteLine("{0}月是春季！", month);
                    quarter = 1;
                    break;
                case 6:
                    Console.WriteLine("{0}月是夏季！", month);
                    quarter = 2;
                    break;
                case 7:
                    Console.WriteLine("{0}月是夏季！", month);
                    quarter = 2;
                    break;
                case 8:
                    Console.WriteLine("{0}月是夏季！", month);
                    quarter = 2;
                    break;
                case 9:
                    Console.WriteLine("{0}月是秋季！", month);
                    quarter = 3;
                    break;
                case 10:
                    Console.WriteLine("{0}月是秋季！", month);
                    quarter = 3;
                    break;
                case 11:
                    Console.WriteLine("{0}月是秋季！", month);
                    quarter = 3;
                    break;
                case 12:
                    Console.WriteLine("{0}月是冬季！", month);
                    quarter = 4;
                    break;
                default:
                    Console.WriteLine("输入错误！");
                    quarter = 0;
                    break;
            }
            return quarter;
        }
    }
}
