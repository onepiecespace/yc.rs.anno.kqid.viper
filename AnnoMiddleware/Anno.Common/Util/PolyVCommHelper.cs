﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Anno.Common.Util
{
    public static class PolyVCommHelper
    {
        public static Task<string> GetPolyVideo(string vid)
        {
            var ptime = Convert.ToInt64((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalMilliseconds).ToString();  // 毫秒级时间戳（13位）
            var userId = "be625c3cf9";
            //6、将字符串进行sha1加密  
            var sha1Sign = PolyVCommHelper.GetShA1(vid, ptime);
            // 5. 将生成的 sign 作为参数一并传给polyv用来获取token
            Dictionary<string, string> parameters = new Dictionary<string, string>
            {
                {"userId", userId},
                {"ptime", ptime},
                {"sign", sha1Sign},
                {"vid", vid},
            };
            var host = "http://api.polyv.net/v2/video";
            string serviceUrl = $"{host}/{userId}/get-video-msg?vid={vid}&sign={sha1Sign}&ptime={ptime}&format=json";
            //string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            //var result = HttpClientUtil.Post(serviceUrl, jsonData);
            byte[] postData = Encoding.UTF8.GetBytes(PolyVCommHelper.BuildQuery(parameters, "utf8"));   //使用utf-8格式组装post参数
            var result = HttpClientUtil.Post(serviceUrl, postData);
            return result;
        }

        public static Task<string> GetImage(string vid)
        {
            //  var vid = RequestString("vid");//选择的视频
            var ptime = Convert.ToInt64((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalMilliseconds).ToString();  // 毫秒级时间戳（13位）
            var userId = "be625c3cf9";
            //6、将字符串进行sha1加密  
            var sha1Sign = GetShA1(vid, ptime);
            // 5. 将生成的 sign 作为参数一并传给polyv用来获取token
            Dictionary<string, string> parameters = new Dictionary<string, string>
                {
                    {"userId", userId},
                    {"ptime", ptime},
                    {"sign", sha1Sign},
                    {"vid", vid},
                    {"t", "1"},
                };
            var host = "http://api.polyv.net/v2/video";
            string serviceUrl = $"{host}/{userId}/get-image?vid={vid}&sign={sha1Sign}&ptime={ptime}&format=json";
            //string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            //var result = HttpClientUtil.Post(serviceUrl, jsonData);
            byte[] postData = Encoding.UTF8.GetBytes(BuildQuery(parameters, "utf8"));   //使用utf-8格式组装post参数
            var result = HttpClientUtil.Post(serviceUrl, postData);
            return result;
            //var response = Newtonsoft.Json.JsonConvert.DeserializeObject<object>(result.Result);
        }
        public static Task<string> delvideos(string vid)
        {
            //  var vid = RequestString("vid");//选择的视频
            var ptime = Convert.ToInt64((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalMilliseconds).ToString();  // 毫秒级时间戳（13位）
            var userId = "be625c3cf9";
            string signStr = "format=json&ptime=" + ptime + "&userId=" + userId + "&vids=" + vid + "UsK0ZLXopt";
            // 3. 尾拼接 secretkey
            //6、将字符串进行sha1加密  
            string signature = SHA1(signStr);

            var sha1Sign = signature;
            // 5. 将生成的 sign 作为参数一并传给polyv用来获取token
            Dictionary<string, string> parameters = new Dictionary<string, string>
                {
                    {"userId", userId},
                    {"ptime", ptime},
                    {"sign", sha1Sign},
                    {"vids", vid},
                    {"deleteType", "1"},//删除方式，默认为11：删除到回收站2：彻底删除
                };
            var host = "http://api.polyv.net/v2/video";
            string serviceUrl = $"{host}/del-videos?vids={vid}&sign={sha1Sign}&userId={userId}&ptime={ptime}&format=json";
            //string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            //var result = HttpClientUtil.Post(serviceUrl, jsonData);
            byte[] postData = Encoding.UTF8.GetBytes(BuildQuery(parameters, "utf8"));   //使用utf-8格式组装post参数
            var result = HttpClientUtil.Post(serviceUrl, postData);
            return result;
            //var response = Newtonsoft.Json.JsonConvert.DeserializeObject<object>(result.Result);
        }
        /// <summary>
        /// 创建用户保利视频分类
        /// </summary>
        /// <param name="cataname"></param>
        /// <param name="parentid"></param>
        /// <returns></returns>
        public static Task<string> CreateVideoCata(string cataname, string parentid)
        {
            //  var vid = RequestString("vid");//选择的视频
            var ptime = Convert.ToInt64((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalMilliseconds).ToString();  // 毫秒级时间戳（13位）
            var userId = "be625c3cf9";
            string signStr = "cataname=" + cataname + "&parentid=" + parentid + "&ptime=" + ptime + "&userid=" + userId + "UsK0ZLXopt";
            // cataname = 242571327103045 & parentid = 1652770575001 & ptime = 1652770689911 & userid = be625c3cf9UsK0ZLXopt

            // 3. 尾拼接 secretkey
            //6、将字符串进行sha1加密  
            string signature = SHA1(signStr);

            var sha1Sign = signature;
            // 5. 将生成的 sign 作为参数一并传给polyv用来获取token
            Dictionary<string, string> parameters = new Dictionary<string, string>
                {
                    {"userid", userId},
                    {"ptime", ptime},
                    {"sign", sha1Sign},
                    {"cataname", cataname},
                    {"parentid", parentid},
                };
            var host = "http://api.polyv.net/v2/video";
            string serviceUrl = $"{host}/{userId}/addCata?sign={sha1Sign}&ptime={ptime}&parentid={parentid}&cataname={cataname}&userid={userId}";
            // http://api.polyv.net/v2/video/be625c3cf9/addCata?sign=889E2FD0236AB6F526C7AE5C156DFC4F33E8E530&ptime=1652770689911&parentid=1652770575001&cataname=242571327103045&userid=be625c3cf9

            //string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            //var result = HttpClientUtil.Post(serviceUrl, jsonData);
            byte[] postData = Encoding.UTF8.GetBytes(BuildQuery(parameters, "utf8"));   //使用utf-8格式组装post参数
            var result = HttpClientUtil.Post(serviceUrl, postData);
            return result;
            //var response = Newtonsoft.Json.JsonConvert.DeserializeObject<object>(result.Result);
        }

        //组装请求参数
        public static string BuildQuery(IDictionary<string, string> parameters, string encode)
        {
            StringBuilder postData = new StringBuilder();
            bool hasParam = false;
            IEnumerator<KeyValuePair<string, string>> dem = parameters.GetEnumerator();
            while (dem.MoveNext())
            {
                string name = dem.Current.Key;
                string value = dem.Current.Value;
                // 忽略参数名或参数值为空的参数
                if (!string.IsNullOrEmpty(name))
                {
                    if (hasParam)
                    {
                        postData.Append("&");
                    }
                    postData.Append(name);
                    postData.Append("=");
                    if (encode == "gb2312")
                    {
                        postData.Append(HttpUtility.UrlEncode(value, Encoding.GetEncoding("gb2312")));
                    }
                    else if (encode == "utf8")
                    {
                        postData.Append(HttpUtility.UrlEncode(value, Encoding.UTF8));
                    }
                    else
                    {
                        postData.Append(value);
                    }
                    hasParam = true;
                }
            }
            return postData.ToString();
        }
        #region 签名处理方法
        public static string GetShA1(string vid, string ptime)
        {
            string signStr = "format=json&ptime=" + ptime + "&vid=" + vid + "UsK0ZLXopt";
            //// 3. 尾拼接 secretkey
            //6、将字符串进行sha1加密  
            string signature = SHA1(signStr);
            return signature;
        }

        private static string SHA1(string decript)
        {
            return CommonHelper.Sha1(decript);
        }

        /// <summary>
        /// MD5 hash加密
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string signMD5(string s)
        {
            var md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            var result = BitConverter.ToString(md5.ComputeHash(UnicodeEncoding.UTF8.GetBytes(s.Trim())));
            return result;
        }
        /// <summary>
        /// 计算MD5值
        /// </summary>
        /// <param name="text">字符串</param>
        /// <returns>字符串</returns>
        public static string Md5(string text)
        {
            String md5 = "";
            MD5 md5_text = MD5.Create();
            byte[] temp = md5_text.ComputeHash(System.Text.Encoding.ASCII.GetBytes(text)); //计算MD5 Hash 值

            for (int i = 0; i < temp.Length; i++)
            {
                md5 += temp[i].ToString("x2"); //转码 每两位转一次16进制
            }
            return md5;
        }
        #endregion
    }
}
