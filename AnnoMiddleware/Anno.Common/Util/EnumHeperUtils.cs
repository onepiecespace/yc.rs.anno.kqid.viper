﻿using Anno.Const;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Anno.Common.Util
{
    public static class EnumHeperUtils
    {
        public static Dictionary<string, string> GetUserPositionAvatar(int? UserType, int TenantType, string Avatar)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            string position = "";
            string avatar = "";
            switch (UserType)
            {
                case 0:
                    position = "系统用户";
                    if (string.IsNullOrEmpty(Avatar))
                    {
                        avatar = AliyunOssOptions.Host + "/GrowthTree/static/avatar/00011.png";
                    }
                    else
                    {
                        if (Avatar.Contains("https://thirdwx.qlogo.cn"))
                        {
                            avatar = Avatar;
                        }
                        else
                        {
                            avatar = AliyunOssOptions.Host + Avatar;
                        }
                    }
                    break;
                case 1:
                    position = "校长";
                    if (string.IsNullOrEmpty(Avatar))
                    {
                        avatar = AliyunOssOptions.Host + "/GrowthTree/static/avatar/00005.png";
                    }
                    else
                    {
                        if (Avatar.Contains("https://thirdwx.qlogo.cn"))
                        {
                            avatar = Avatar;
                        }
                        else
                        {
                            avatar = AliyunOssOptions.Host + Avatar;
                        }
                    }
                    break;
                case 2:

                    if (TenantType == 0)
                    {
                        position = "校长";
                    }
                    else
                    {
                        position = "教务";
                    }
                    if (string.IsNullOrEmpty(Avatar))
                    {
                        avatar = AliyunOssOptions.Host + "/GrowthTree/static/avatar/00003.png";
                    }
                    else
                    {
                        if (Avatar.Contains("https://thirdwx.qlogo.cn"))
                        {
                            avatar = Avatar;
                        }
                        else
                        {
                            avatar = AliyunOssOptions.Host + Avatar;
                        }
                    }
                    break;
                case 3:
                    position = "教师";
                    if (string.IsNullOrEmpty(Avatar))
                    {
                        avatar = AliyunOssOptions.Host + "/GrowthTree/static/avatar/00008.png";
                    }
                    else
                    {
                        if (Avatar.Contains("https://thirdwx.qlogo.cn"))
                        {
                            avatar = Avatar;
                        }
                        else
                        {
                            avatar = AliyunOssOptions.Host + Avatar;
                        }
                    }
                    break;
                case 4:
                    position = "家长";
                    if (string.IsNullOrEmpty(Avatar))
                    {
                        avatar = AliyunOssOptions.Host + "/GrowthTree/static/avatar/00010.png";
                    }
                    else
                    {
                        if (Avatar.Contains("https://thirdwx.qlogo.cn"))
                        {
                            avatar = Avatar;
                        }
                        else
                        {
                            avatar = AliyunOssOptions.Host + Avatar;
                        }
                    }
                    break;
                default:
                    break;
            }
            result.Add("position", position);
            result.Add("avatar", avatar);
            return result;
        }
        public static Dictionary<string, string> GetUserPositionAvatar(int? UserType, string Avatar)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            string position = "";
            string avatar = "";
            switch (UserType)
            {
                case 0:
                    position = "系统用户";
                    if (string.IsNullOrEmpty(Avatar))
                    {
                        avatar = AliyunOssOptions.Host + "/GrowthTree/static/avatar/00011.png";
                    }
                    else
                    {
                        if (Avatar.Contains("https://thirdwx.qlogo.cn"))
                        {
                            avatar = Avatar;
                        }
                        else
                        {
                            avatar = AliyunOssOptions.Host + Avatar;
                        }
                    }
                    break;
                case 1:
                    position = "校长";
                    if (string.IsNullOrEmpty(Avatar))
                    {
                        avatar = AliyunOssOptions.Host + "/GrowthTree/static/avatar/00005.png";
                    }
                    else
                    {
                        if (Avatar.Contains("https://thirdwx.qlogo.cn"))
                        {
                            avatar = Avatar;
                        }
                        else
                        {
                            avatar = AliyunOssOptions.Host + Avatar;
                        }
                    }
                    break;
                case 2:
                     position = "教务";
                    if (string.IsNullOrEmpty(Avatar))
                    {
                        avatar = AliyunOssOptions.Host + "/GrowthTree/static/avatar/00003.png";
                    }
                    else
                    {
                        if (Avatar.Contains("https://thirdwx.qlogo.cn"))
                        {
                            avatar = Avatar;
                        }
                        else
                        {
                            avatar = AliyunOssOptions.Host + Avatar;
                        }
                    }
                    break;
                case 3:
                    position = "教师";
                    if (string.IsNullOrEmpty(Avatar))
                    {
                        avatar = AliyunOssOptions.Host + "/GrowthTree/static/avatar/00008.png";
                    }
                    else
                    {
                        if (Avatar.Contains("https://thirdwx.qlogo.cn"))
                        {
                            avatar = Avatar;
                        }
                        else
                        {
                            avatar = AliyunOssOptions.Host + Avatar;
                        }
                    }
                    break;
                case 4:
                    position = "家长";
                    if (string.IsNullOrEmpty(Avatar))
                    {
                        avatar = AliyunOssOptions.Host + "/GrowthTree/static/avatar/00010.png";
                    }
                    else
                    {
                        if (Avatar.Contains("https://thirdwx.qlogo.cn"))
                        {
                            avatar = Avatar;
                        }
                        else
                        {
                            avatar = AliyunOssOptions.Host + Avatar;
                        }
                    }
                    break;
                default:
                    break;
            }
            result.Add("position", position);
            result.Add("avatar", avatar);
            return result;
        }
        public static Dictionary<string, string> getstatusstr(int? status)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            int vidstatus = 0;//等待审核 1已发布 2审核失败-1 已删除
            string statusstr = "";
            switch (status)
            {
                case 10:
                    statusstr = "等待编码";
                    break;
                case 20:
                    statusstr = "正在编码";
                    break;
                case 40:
                    statusstr = "视频处理失败";
                    vidstatus = 2;
                    break;
                case 50:
                    statusstr = "等待审核";
                    break;
                case 51:
                    statusstr = "审核不通过";
                    vidstatus = 2;
                    break;
                case 60:
                    statusstr = "已发布";
                    vidstatus = 1;
                    break;
                case 61:
                    statusstr = "已发布";
                    vidstatus = 1;
                    break;
                case -1:
                    statusstr = "已删除";
                    vidstatus = -1;
                    break;
                default:
                    break;
            }
            result.Add("vidstatus", vidstatus.ToString());
            result.Add("statusstr", statusstr);
            return result;
        }
        public static string gettermstr(int? quarter)
        {
            string termstr = "";
            switch (quarter)
            {
                case 1:
                    termstr = "春季班";
                    break;
                case 2:
                    termstr = "暑假班";
                    break;
                case 3:
                    termstr = "秋季班";
                    break;
                case 4:
                    termstr = "寒假班";
                    break;
                default:
                    break;
            }
            return termstr;
        }
        public static string gettermshotstr(int? quarter)
        {
            string termstr = "";
            switch (quarter)
            {
                case 1:
                    termstr = "春";
                    break;
                case 2:
                    termstr = "暑";
                    break;
                case 3:
                    termstr = "秋";
                    break;
                case 4:
                    termstr = "寒";
                    break;
                default:
                    break;
            }
            return termstr;
        }

        public static string getweekstr(int? week)
        {
            string weekstr = "";
            switch (week)
            {
                case 1:
                    weekstr = "周一";
                    break;
                case 2:
                    weekstr = "周二";
                    break;
                case 3:
                    weekstr = "周三";
                    break;
                case 4:
                    weekstr = "周四";
                    break;
                case 5:
                    weekstr = "周五";
                    break;
                case 6:
                    weekstr = "周六";
                    break;
                case 7:
                    weekstr = "周日";
                    break;
                default:
                    break;
            }
            return weekstr;
        }
        public static Dictionary<string, string> getclassLevels(int? classLevel)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            string classLevelstr = "";
            string classLevellogo = "";
            string classLogo = "";
            switch (classLevel)
            {
                case 1:
                    classLevelstr = "一级";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv1.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv1.png";
                    break;
                case 2:
                    classLevelstr = "二级";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv2.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv2.png";
                    break;
                case 3:
                    classLevelstr = "三级";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv3.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv3.png";
                    break;
                case 4:
                    classLevelstr = "四级";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv4.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv4.png";
                    break;
                case 5:
                    classLevelstr = "五级";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv5.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv5.png";
                    break;
                case 6:
                    classLevelstr = "六级";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv6.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv6.png";
                    break;
                case 7:
                    classLevelstr = "七级";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv7.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv7.png";
                    break;
                case 8:
                    classLevelstr = "八级";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv8.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv8.png";
                    break;
                case 9:
                    classLevelstr = "九级";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv9.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv9.png";
                    break;
                case 10:
                    classLevelstr = "十级";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv10.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv10.png";
                    break;
                case 11:
                    classLevelstr = "假期班";
                    classLevellogo = AliyunOssOptions.Host + "/GrowthTree/static/levels/lv11.png";
                    classLogo = AliyunOssOptions.Host + "/GrowthTree/static/classes/lv11.png";
                    break;
                default:
                    break;
            }
            result.Add("Levels", classLevelstr);
            result.Add("Levellogo", classLevellogo);
            result.Add("ClassLogo", classLogo);
            return result;
        }
        public static string getLevelStr(int? classLevel)
        {

            string classLevelstr = "";
            switch (classLevel)
            {
                case 1:
                    classLevelstr = "口才一级班";
                    break;
                case 2:
                    classLevelstr = "口才二级班";
                    break;
                case 3:
                    classLevelstr = "口才三级班";
                    break;
                case 4:
                    classLevelstr = "口才四级班";
                    break;
                case 5:
                    classLevelstr = "口才五级班";
                    break;
                case 6:
                    classLevelstr = "口才六级班";
                    break;
                case 7:
                    classLevelstr = "口才七级班";
                    break;
                case 8:
                    classLevelstr = "口才八级班";
                    break;
                case 9:
                    classLevelstr = "口才九级班";
                    break;
                case 10:
                    classLevelstr = "口才十级班";
                    break;
                case 11:
                    classLevelstr = "假期班";
                    break;
                default:
                    break;
            }
            return classLevelstr;
        }
    }
}
