﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using Hx.Extensions;

namespace Anno.Common
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="C"></typeparam>
    public class Compare<T, C> : IEqualityComparer<T>
    {
        private Func<T, C> _getField;
        public Compare(Func<T, C> getfield)
        {
            this._getField = getfield;
        }
        public bool Equals(T x, T y)
        {
            return EqualityComparer<C>.Default.Equals(_getField(x), _getField(y));
        }
        public int GetHashCode(T obj)
        {
            return EqualityComparer<C>.Default.GetHashCode(this._getField(obj));
        }
    }

    public static class CommonHelper
    {
        /// <summary>
        /// 自定义Distinct扩展方法
        /// </summary>
        /// <typeparam name="T">要去重的对象类</typeparam>
        /// <typeparam name="C">自定义去重的字段类型</typeparam>
        /// <param name="source">要去重的对象</param>
        /// <param name="getfield">获取自定义去重字段的委托</param>
        /// <returns></returns>
        public static IEnumerable<T> MyDistinct<T, C>(this IEnumerable<T> source, Func<T, C> getfield)
        {
            return source.Distinct(new Compare<T, C>(getfield));
        }

        public static string Sha1(string content)
        {
            return Sha1(content, Encoding.UTF8);
        }

        public static string Sha1(string content, Encoding encode)
        {
            try
            {
                var sha1 = new SHA1CryptoServiceProvider();//创建SHA1对象
                var bytes_in = encode.GetBytes(content);//将待加密字符串转为byte类型
                var bytes_out = sha1.ComputeHash(bytes_in);//Hash运算
                sha1.Dispose();//释放当前实例使用的所有资源
                var result = BitConverter.ToString(bytes_out);//将运算结果转为string类型
                result = result.Replace("-", "");//替换并转为大写
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
        }

        /// <summary>
        /// 获取Dictionary中不重复的随机Dictionary
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dict">原dictionary</param>
        /// <param name="count">返回随机个数(如果dict总个数少于count 则返回dict总个数)</param>
        /// <returns></returns>
        public static Dictionary<TKey, TValue> RandomValues<TKey, TValue>(Dictionary<TKey, TValue> dict, int count)
        {
            Random rand = new Random();
            Dictionary<TKey, TValue> dic = new Dictionary<TKey, TValue>();
            int size = dict.Count;
            count = count > size ? size : count;
            List<TKey> values = Enumerable.ToList(dict.Keys);
            while (dic.Count < count)
            {
                TKey tk = values[rand.Next(size)];
                if (!dic.Keys.Contains(tk))
                {
                    dic[tk] = dict[tk];
                }
            }
            return dic;
        }

        public static string HMACSHA256Sign(string value, string key)
        {
            var bkey = Encoding.UTF8.GetBytes(key);
            return HMACSHA256Sign(value, bkey);
        }
        public static string HMACSHA256Sign(string value, byte[] key)
        {
            using (var hmac = new HMACSHA256(key))
            {
                var bytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(value));
                var sb = new StringBuilder();
                bytes.ForEach(b =>
                {
                    sb.Append(string.Format("0:X2"));
                });
                return sb.ToString();
            }
        }
        public static string AesDecrypt(string data, string key)
        {
            return AesDecrypt(data, key, key);
        }

        public static string AesDecrypt(string data, string key, string iv)
        {
            if (string.IsNullOrEmpty(data))
            {
                throw new ArgumentException("data is empty.");
            }

            if (key == null || iv == null)
            {
                throw new ArgumentException("Key/Iv is null.");
            }

            byte[] bkey = System.Text.Encoding.UTF8.GetBytes(key);
            byte[] biv = System.Text.Encoding.UTF8.GetBytes(iv);

            using (var rijndaelManaged = new RijndaelManaged()
            {
                Key = bkey,
                IV = biv,
                KeySize = 256,
                BlockSize = 128,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            })
            {
                using (var transform = rijndaelManaged.CreateDecryptor(bkey, biv))
                {
                    var inputBytes = StrToHexByte(data);
                    var encryptedBytes = transform.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
                    return Encoding.UTF8.GetString(encryptedBytes);
                }
            }
        }


        public static byte[] StrToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        public static string ToHexString(byte[] bytes)
        {
            string hexString = string.Empty;
            if (bytes != null)
            {
                StringBuilder strB = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    strB.Append(bytes[i].ToString("X2"));
                }
                hexString = strB.ToString();
            }
            return hexString;
        }

        public static int GetRandNum(int min, int max)
        {
            Random r = new Random(Guid.NewGuid().GetHashCode());
            return r.Next(min, max);
        }
        /// <summary>
        /// 获取时间戳
        /// </summary>
        /// <returns></returns>
        public static long GetTimeStamp()
        {
            TimeSpan ts = DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds);
        }

    }


}
