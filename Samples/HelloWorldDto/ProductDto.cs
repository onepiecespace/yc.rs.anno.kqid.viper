﻿using System;

namespace HelloWorldDto
{
    public class ProductDto
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public double Price { get; set; }
        public double Amount { get { return Price * Number; } }
        public string CountryOfOrigin { get; set; }
        public OrderReqDto OrderInfo { get; set; }
    }
    /// <summary>
    /// 商品请求实体
    /// </summary>
    public class ProductReqDto
    {
        public string productName { get; set; }
        public int number { get; set; }
        public string country { get; set; }

    }
    /// <summary>
    /// 订单请求实体
    /// </summary>
    public class OrderReqDto
    {
        public string orderNo { get; set; }
        public int buynumber { get; set; }
        public string adress { get; set; }

    }
}
